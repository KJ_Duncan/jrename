/*
DVNA 2020, Sushi In Tokyo, All My Friends, viewed 08 July 2021,
          <https://open.spotify.com/track/2TlDrAlvvacxMpl5SOItGh?si=31b2566b7efa4d85>

$ sqlite3 brooklyn.db

sqlite> .mode box
sqlite> .databases
sqlite> .read brooklyn.sql
sqlite> .tables
sqlite> select * from jmaster;
sqlite> select * from junstructured;
sqlite> .exit
 */
DROP TABLE IF EXISTS junstructured;
DROP TABLE IF EXISTS janalytical;
DROP TABLE IF EXISTS joperational;
DROP TABLE IF EXISTS jmaster;

/* <https://www.sqlite.org/c3ref/open.html#urifilenameexamples> */
CREATE TABLE junstructured (
  jun_id  INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  jun_log TEXT    NOT NULL DEFAULT 'All good',

  CONSTRAINT jco_ck1 CHECK ( 0 < LENGTH( jun_log ) )
);

CREATE TABLE janalytical (
  jan_id    INTEGER      NOT NULL PRIMARY KEY AUTOINCREMENT,
  jan_stats VARCHAR(255) NOT NULL DEFAULT 'range:mean:median:mode',

  CONSTRAINT jan_ck1 CHECK ( 0 < LENGTH( jan_stats ) )
);

CREATE TABLE joperational (
  jop_id  INTEGER      NOT NULL PRIMARY KEY AUTOINCREMENT,
  jop_uri VARCHAR(255) NOT NULL DEFAULT 'success',

  CONSTRAINT jop_ck1 CHECK ( 0 < LENGTH( jop_uri ) )
);

CREATE TABLE jmaster (
  jma_id     INTEGER      NOT NULL PRIMARY KEY AUTOINCREMENT,
  jma_regex  VARCHAR(255) NOT NULL DEFAULT '[^0-9A-Z_a-z]',
  jma_dedupe VARCHAR(55)  NOT NULL DEFAULT '(_)+',
  jma_ext    VARCHAR(6)   NOT NULL DEFAULT 'all',

  CONSTRAINT jma_ck1 CHECK ( 0 < LENGTH( jma_regex ) ),
  CONSTRAINT jma_ck2 CHECK ( 0 < LENGTH( jma_dedupe ) ),
  CONSTRAINT jma_ck3 CHECK ( 0 < LENGTH( jma_ext ) )
);

INSERT INTO junstructured DEFAULT VALUES;
INSERT INTO janalytical   DEFAULT VALUES;
INSERT INTO joperational  DEFAULT VALUES;
INSERT INTO jmaster       DEFAULT VALUES;

INSERT INTO junstructured ( jun_log ) VALUES ( 'ExceptionalFunction(ace.getMessage())' );
INSERT INTO junstructured ( jun_log ) VALUES ( 'ioe.printStackTrace()' );
INSERT INTO junstructured ( jun_log ) VALUES ( 'Throwable.getCause()' );

INSERT INTO janalytical ( jan_stats ) VALUES ( '5:4:3:2' );
INSERT INTO janalytical ( jan_stats ) VALUES ( '15:3:5:12' );
INSERT INTO janalytical ( jan_stats ) VALUES ( '3:2:1:0' );

INSERT INTO joperational ( jop_uri ) VALUES ( 'file://path/to/parent/directory' );
INSERT INTO joperational ( jop_uri ) VALUES ( 'SUCCESS' );
INSERT INTO joperational ( jop_uri ) VALUES ( 'file://path/to/parent/directory/' );

INSERT INTO jmaster ( jma_regex, jma_dedupe, jma_ext ) VALUES ( '[^A-Z]', '(_)+', 'all' );
INSERT INTO jmaster ( jma_regex, jma_dedupe, jma_ext ) VALUES ( '[^0-9-a-z]', '(-)+', 'mp4' );
INSERT INTO jmaster ( jma_regex, jma_dedupe, jma_ext ) VALUES ( '[^0-5~a-e]', '(~)+', 'jpg' );

UPDATE OR IGNORE   joperational SET jop_uri = 'file://parent/directory/ignore'   WHERE joperational.jop_id = 101;
UPDATE OR ABORT    joperational SET jop_uri = 'file://parent/directory/abort'    WHERE joperational.jop_id = 102;
UPDATE OR FAIL     joperational SET jop_uri = 'file://parent/directory/fail'     WHERE joperational.jop_id = 103;
UPDATE OR REPLACE  joperational SET jop_uri = 'file://parent/directory/replace'  WHERE joperational.jop_id = 104;
UPDATE OR ROLLBACK joperational SET jop_uri = 'file://parent/directory/rollback' WHERE joperational.jop_id = 105;

UPDATE joperational SET jop_uri = 'file://parent/directory/update' WHERE joperational.jop_id = 3;


-- replace ? with your preferred value

-- INSERT INTO junstructured ( jun_log )   VALUES ( ? );
-- INSERT INTO janalytical   ( jwa_stats ) VALUES ( ? );
-- INSERT INTO joperational  ( jop_uri )   VALUES ( ? );
-- INSERT INTO jmaster ( jma_regex, jma_dedupe, jma_ext ) VALUES ( ?, ?, ? );

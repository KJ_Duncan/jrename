module au.kjd.brooklyn {
  requires java.base;
  requires java.logging;
  requires java.sql;
  requires org.apache.logging.log4j;
  /*
   * Public API
   *  - Public types in exported packages
   *  - Public and protected fields
   *  - Public and protected methods
   *
   * A public type is never accessible,
   * but only to a particular module.
   *
   * Public is no longer public.
   *
   * Accessibility: Defining public API, The Java Module System, chpt 3, pp.68-71
   *
   * exports au.kjd.brooklyn to au.kjd.jrename;
   */
}

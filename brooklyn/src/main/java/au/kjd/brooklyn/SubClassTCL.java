package au.kjd.brooklyn;

import java.sql.*;

import static java.lang.String.*;
import static java.sql.DriverManager.*;

// ------------------------------------------------------------------------------------------------- \\
/* a single connection open for batch processing for the
 * duration of the program.
 *
 * an abstract class can hold interface's as data fields,
 * we can implement a bridge design pattern instead of
 * using a java module service implementation.
 */
public final class SubClassTCL {

  /* TEST: Connection volatiles' write before read thread safety */
  private static final String JDBC = "jdbc:sqlite:%s";
  private static volatile Connection connection;
  private static String url;

  private SubClassTCL() {
  }

  public static void init(String path) throws SQLException {
    url = path;
    connection();
  }

  public static Connection connect() throws SQLException {
    /* timeout in seconds to wait for valid connection */
    if (null != connection && !connection.isValid(1)) { disconnect(); connection(); }
    if (null == connection) connection();
    return connection;
  }

  public static void disconnect() {
    assert null != connection;
    try { connection.close(); } catch (SQLException ignore) { /* JVM is shutting down */ }
  }

  private static void connection() throws SQLException {
    connection = getConnection(format(JDBC, url));
    connection.setAutoCommit(false);
  }

  /*
  private static void connection() throws SQLException {
    connection = getConnection(BROOKLYN_DB.getUrl());
    connection.setAutoCommit(false);
  }
   */
}

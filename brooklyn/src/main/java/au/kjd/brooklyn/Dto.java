package au.kjd.brooklyn;

sealed interface Dto permits JUnstructuredDTO, JAnalyticalDTO, JOperationalDTO, JMasterDTO { }

/* Unstructured data */
final record JUnstructuredDTO(Integer junId, String junLog) implements Dto {
  @Override public String toString() {
    return """
        JUnstructuredDTO{
          jun_id=%s,
          jun_log=%s
        }""".formatted(junId, junLog);
  }
}

/* Data warehouse (OLAP) */
final record JAnalyticalDTO(int janId, String janStats) implements Dto {
  @Override public String toString() {
    return """
      JAnalyticalDTO{
        jan_id=%s,
        jan_stats=%s
      }""".formatted(janId, janStats);
  }
}

/* Operational data (OLTP) */
final record JOperationalDTO(int jopId, String jopUri) implements Dto {
  @Override public String toString() {
    return """
      JOperationalDTO{
        jop_id=%s,
        jop_uri=%s
      }""".formatted(jopId, jopUri);
  }
}

/* Master data */
final record JMasterDTO(int jmaId, String jmaRegex,
                        String jmaDedupe, String jmaExt) implements Dto {
  @Override public String toString() {
    return """
      JMasterDTO{
        jma_id=%s,
        jma_regex=%s,
        jma_dedupe=%s,
        jma_ext=%s
      }""".formatted(jmaId, jmaRegex, jmaDedupe, jmaExt);
  }
}

package au.kjd.brooklyn;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Savepoint;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.List;

import static au.kjd.brooklyn.SubClassTCL.connect;

/* 4.3 Two-tier Model, chpt 4, p.17
 *
 * Oracle 2006, JSR-000221 JDBC 4.3 Maintenance Release 3 Specification, viewed 18 August 2021,
 *              <https://download.oracle.com/otndocs/jcp/jdbc-4_3-mrel3-spec/index.html>
 *
 */
// ------------------------------------------------------------------------------------------------- \\
// Inheritance
public abstract class SQLCommands {

  // DataDomain ordinal values 0,1,2,3,4,5,6,7,8,9,10
  private static final int INSERT = 2;
  private static final int SELECT = 3;
  private static final int UPDATE = 4;
  private static final int ID_5 = 5;
  private static final int ATTR = 6;
  private static final int ID_7 = 7;
  private static final int REGEX = 8;
  private static final int DEDUPE = 9;
  private static final int EXT = 10;

  /* a wrapper function to perform an abstraction at the call site (dml) */
  private static String getAttribute(final DataDomain table) {
    if (table instanceof JUnstructured jun)     { return jun.getAttribute(); }
    else if (table instanceof JAnalytical jan)  { return jan.getAttribute(); }
    else if (table instanceof JOperational jop) { return jop.getAttribute(); }
    else if (table instanceof JMaster jma)      { return jma.getAttribute(); }
    else throw new IllegalStateException("No data in the domain");
  }

  // ------------------------------------------------------------------------------------------------- \\
  /* Transaction Control Language (TCL) */
  private sealed interface BrooklynTCL permits BrooklynDDL, BrooklynDML {

  /*
    if we use either INSERT OR FAIL or UPDATE OR FAIL will eliminate rollback.
    will be implemented as a batch process on directory changes, the implementation
    concern are for log messages must make it into JUnstructured or how will the user
    know: where, why, and how to fix in a file hierarchy.

    Void noop = (boolean) ? commit(ps.getConnection()) : rollback(ps.getConnection());
  */

    private static Consumer<Connection> commit() {
      return conn -> {
        try {
          conn.commit();
        } catch (SQLException sqle) {
          throw new RuntimeException(sqle);
        }
      };
    }

    private static BiConsumer<Connection, Savepoint> rollback() {
      return (conn, save) -> {
        try {
          conn.rollback(save);
          conn.commit();
        } catch (SQLException sqle) {
          throw new RuntimeException(sqle);
        }
      };
    }

    private static Function<Connection, Savepoint> savepoint() {
      return conn -> {
        try {
          return conn.setSavepoint();
        } catch (SQLException sqle) {
          throw new RuntimeException(sqle);
        }
      };
    }
  }

  // ------------------------------------------------------------------------------------------------- \\
  /* Data Definition Language (DDL) */
  public static final class BrooklynDDL extends SQLCommands implements BrooklynTCL {
    private BrooklynDDL() {}

    // data definition language (ddl): create, drop, and alter
    public static void create(final String sql) throws SQLException {
      final PreparedStatement ps = connect().prepareStatement(sql);
      ps.execute();
      BrooklynTCL.commit().accept(connect());
    }

    public static void drop(final String sql) throws SQLException {
      create(sql);
    }

    public static void alter() throws SQLException {
      throw new SQLException("sqlite is different in that it stores the schema in the sqlite_schema table as the original text of the CREATE statements that define the schema.");
    }
  }

  // ------------------------------------------------------------------------------------------------- \\
  /* Data Query Language (DQL) / Data Access Object (DAO)
   *
   * Sun Microsystems 2002, Core J2EE Patterns - Data Access Object, viewed 05 September 2020,
   *                        <https://www.oracle.com/java/technologies/dataaccessobject.html>
   */
  public static final class BrooklynDQL extends SQLCommands {
    private BrooklynDQL() {}

    public static List<Dto> select(final DataDomain[] data,
                             final DataServices services,
                             final int PK_1) throws SQLException {

      List<Dto> dtos = new ArrayList<Dto>();

      final PreparedStatement ps = connect()
          .prepareStatement(getAttribute(data[SELECT]));

      ps.setInt(1, PK_1);
      ResultSet rs = ps.executeQuery();

      while (rs.next()) {
        switch (services) {
          case JUNSTRUCTURED -> dtos.add(new JUnstructuredDTO(rs.getInt(getAttribute(data[ID_5])),
              rs.getString(getAttribute(data[ATTR]))));

          case JANALYTICAL -> dtos.add(new JAnalyticalDTO(rs.getInt(getAttribute(data[ID_5])),
              rs.getString(getAttribute(data[ATTR]))));

          case JOPERATIONAL -> dtos.add(new JOperationalDTO(rs.getInt(getAttribute(data[ID_5])),
              rs.getString(getAttribute(data[ATTR]))));

          case JMASTER -> dtos.add(new JMasterDTO(rs.getInt(getAttribute(data[ID_7])),
              rs.getString(getAttribute(data[REGEX])),
              rs.getString(getAttribute(data[DEDUPE])),
              rs.getString(getAttribute(data[EXT]))));
        }
      }
      rs.close();
      ps.clearParameters();
      return dtos;
    }
  }

  // ------------------------------------------------------------------------------------------------- \\
  /* Data Manipulation Language (DML) / Data Access Object (DAO) */
  public static final class BrooklynDML extends SQLCommands implements BrooklynTCL {
    private BrooklynDML() {}

    // data manipulation language (dml): insert, update, delete, conditionals, and aggregate functions
    public static void insert(final DataDomain[] data,
                        final DataServices services,
                        final String value) throws SQLException {

      if ("JMASTER".equals(services.name())) throw new SQLException("Incorrect INSERT method invocation");

      final PreparedStatement ps = connect()
          .prepareStatement(getAttribute(data[INSERT]));

      ps.setString(1, value);
      ps.execute();

      transaction(ps, BrooklynTCL.savepoint().apply(connect()));
    }

    public static void insert(final DataDomain[] data,
                        final DataServices services,
                        final String attr_2,
                        final String attr_3,
                        final String attr_4) throws SQLException {

      if (!"JMASTER".equals(services.name())) throw new SQLException("Incorrect INSERT method invocation");

      final PreparedStatement ps = connect()
          .prepareStatement(getAttribute(data[INSERT]));

      ps.setString(1, attr_2);
      ps.setString(2, attr_3);
      ps.setString(3, attr_4);
      ps.execute();

      transaction(ps, BrooklynTCL.savepoint().apply(connect()));
    }

    // only joperational calls this function to update jop_uri on directory changes which causes a trigger
    public static void update(final DataDomain[] data,
                        final int PK_1,
                        final String attr_N) throws SQLException {

      final PreparedStatement ps = connect()
          .prepareStatement(getAttribute(data[UPDATE]));

      ps.setString(1, attr_N);
      ps.setInt(2, PK_1);
      ps.execute();

      transaction(ps, BrooklynTCL.savepoint().apply(connect()));
    }

    public static void update(final DataDomain[] data,
                        final int ord,
                        final int PK_1,
                        final String attr_N) throws SQLException {

      final PreparedStatement ps = connect()
          .prepareStatement(getAttribute(data[ord]));

      ps.setString(1, attr_N);
      ps.setInt(2, PK_1);
      ps.execute();

      transaction(ps, BrooklynTCL.savepoint().apply(connect()));
    }

    public static void delete() throws SQLException { throw new SQLException("Operation not supported"); }

    // helper function
    private static void transaction(PreparedStatement ps, Savepoint sp) throws SQLException {
      try { BrooklynTCL.commit().accept(connect()); }
      catch (SQLException sqle) { BrooklynTCL.rollback().accept(connect(), sp); throw new RuntimeException(sqle); }
      finally { ps.clearParameters(); }
    }
  }
}


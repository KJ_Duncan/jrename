package au.kjd.brooklyn;

sealed interface BrooklynResult permits BSuccess, BFailure {
  static BrooklynResult resultsFrom(Object obj) {
    try { return new BSuccess<>(obj); }
    catch (Throwable ex) { return new BFailure<>(ex); }
  }
}

final record BSuccess<S>(S success) implements BrooklynResult { }

final record BFailure<F extends Throwable>(F failure) implements BrooklynResult { }

package au.kjd.brooklyn;

sealed interface DataDomain permits JUnstructured, JAnalytical, JOperational, JMaster { }

// ---------------------------------------------------------------------------------------------- \\

enum JUnstructured implements DataDomain { // ordinal value 0,1,2,3,4,5,6
  JUN_DROP("DROP TABLE IF EXISTS junstructured"),
  JUN_CREATE("""
      CREATE TABLE junstructured (
        jun_id  INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        jun_log TEXT    NOT NULL DEFAULT 'All good',

        CONSTRAINT jun_ck1 CHECK ( 0 < LENGTH( jun_log ) )
      );
      """),
  JUN_INSERT("INSERT INTO junstructured ( jun_log ) VALUES ( ? )"),
  JUN_SELECT("SELECT jun_id, jun_log FROM junstructured WHERE jun_id = ?"),
  JUN_UPDATE("UPDATE OR FAIL junstructured SET jun_log = ? WHERE junstructured.jun_id = ?"),
  JUN_ID("jun_id"),
  JUN_LOG("jun_log");

  private final String attribute;
  JUnstructured(String attr) { this.attribute = attr; }
  String getAttribute() { return attribute; }
}

enum JAnalytical implements DataDomain {
  JAN_DROP("DROP TABLE IF EXISTS janalytical"),
  JAN_CREATE("""
      CREATE TABLE janalytical (
        jan_id    INTEGER      NOT NULL PRIMARY KEY AUTOINCREMENT,
        jan_stats VARCHAR(255) NOT NULL DEFAULT '0:0:0:0',

        CONSTRAINT jan_ck1 CHECK ( 0 < LENGTH( jan_stats ) )
      );
      """),
  JAN_INSERT("INSERT INTO janalytical ( jan_stats ) VALUES ( ? )"),
  JAN_SELECT("SELECT jan_id, jan_stats FROM janalytical WHERE jan_id = ?"),
  JAN_UPDATE("UPDATE OR FAIL janalytical SET jan_stats = ? WHERE janalytical.jan_id = ?"),
  JAN_ID("jan_id"),
  JAN_STATS("jan_stats");

  private final String attribute;
  JAnalytical(String attr) { this.attribute = attr; }
  String getAttribute() { return attribute; }
}

/* TEST: JMH on JOperational UPDATE OR IGNORE|ABORT|FAIL|REPLACE|ROLLBACK, compile time string constants vs runtime object creation and garbage collection
         idea sparked by: <https://github.com/JetBrains/kotless/blob/master/schema/src/main/kotlin/io/kotless/resource/Lambda.kt#L37> */
enum JOperational implements DataDomain {
  JOP_DROP("DROP TABLE IF EXISTS joperational"),
  JOP_CREATE("""
      CREATE TABLE joperational (
        jop_id  INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        jop_uri TEXT    NOT NULL DEFAULT 'success',

        CONSTRAINT jop_ck1 CHECK ( 0 < LENGTH( jop_uri ) )
      );
      """),
  JOP_INSERT("INSERT INTO joperational ( jop_uri ) VALUES ( ? )"),
  JOP_SELECT("SELECT jop_id, jop_uri FROM joperational WHERE jop_id = ?"),
  JOP_UPDATE("UPDATE OR FAIL joperational SET jop_uri = ? WHERE joperational.jop_id = ?"),
  JOP_ID("jop_id"),
  JOP_URI("jop_uri");

  private final String attribute;
  JOperational(String attr) { this.attribute = attr; }
  String getAttribute() { return attribute; }
}

enum JMaster implements DataDomain { // ordinal values 0,1,2,3,4,5,6,7,8,9,10
  JMA_DROP("DROP TABLE IF EXISTS jmaster"),
  JMA_CREATE("""
      CREATE TABLE jmaster (
        jma_id     INTEGER      NOT NULL PRIMARY KEY AUTOINCREMENT,
        jma_regex  VARCHAR(255) NOT NULL DEFAULT '[^0-9A-Z_a-z]',
        jma_dedupe VARCHAR(55)  NOT NULL DEFAULT '(_)+',
        jma_ext    VARCHAR(6)   NOT NULL DEFAULT 'all',

        CONSTRAINT jma_ck1 CHECK ( 0 < LENGTH( jma_regex ) ),
        CONSTRAINT jma_ck2 CHECK ( 0 < LENGTH( jma_dedupe ) ),
        CONSTRAINT jma_ck3 CHECK ( 0 < LENGTH( jma_ext ) )
      );
      """),
  JMA_INSERT("INSERT INTO jmaster ( jma_regex, jma_dedupe, jma_ext ) VALUES ( ?, ?, ? )"),
  JMA_SELECT("SELECT jma_id, jma_regex, jma_dedupe, jma_ext FROM jmaster WHERE jma_id = ?"),
  JMA_UPDATE_REGEX("UPDATE jmaster SET jma_regex = ? WHERE jma_id = ?"),
  JMA_UPDATE_DEDUPE("UPDATE jmaster SET jma_dedupe = ? WHERE jma_id = ?"),
  JMA_UPDATE_EXT("UPDATE jmaster SET jma_ext = ? WHERE jma_id = ?"),
  JMA_ID("jma_id"),
  JMA_REGEX("jma_regex"),
  JMA_DEDUPE("jma_dedupe"),
  JMA_EXT("jma_ext");

  private final String attribute;
  JMaster(String attr) { this.attribute = attr; }
  String getAttribute() { return attribute; }
}

/* Factory Method design pattern (BrooklynDQL)
 * ref: <https://javadevcentral.com/factory-method>
 */
enum DataServices { JUNSTRUCTURED, JANALYTICAL, JOPERATIONAL, JMASTER }


package au.kjd.brooklyn;

sealed interface Database permits Brooklyn { }

// REMOVE: enum Brooklyn for a dynamic url path
enum Brooklyn implements Database {
  BROOKLYN_DB("jdbc:sqlite:brooklyn.db");

  private final String url;
  Brooklyn(String url) { this.url = url; }
  String getUrl() { return this.url; }
}

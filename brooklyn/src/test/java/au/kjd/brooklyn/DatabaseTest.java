package au.kjd.brooklyn;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DatabaseTest {

  @Test
  void testBrooklynDatabaseUrl() {
    assertEquals("jdbc:sqlite:brooklyn.db", Brooklyn.BROOKLYN_DB.getUrl());
    assertEquals(1, Brooklyn.values().length);
    assertEquals(0, Brooklyn.values()[0].ordinal());
  }
}

package au.kjd.brooklyn;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatObject;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

/* ./gradlew :brooklyn:test --tests "au.kjd.brooklyn.SQLCommandsTest" */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class SQLCommandsTest {

  private static final Logger logger = LogManager.getLogger();

//  static SubClassDDL ddl;
  static volatile Connection conn;

  @BeforeAll static void startup() throws SQLException {

    // creates an empty database: jrename/brooklyn/brooklyn.db
    SubClassTCL.init("brooklyn.db");
    conn = SubClassTCL.connect();
  }

  @AfterAll static void shutDown() throws IOException, SQLException {

    conn.close();
    Files.deleteIfExists(Paths.get("brooklyn.db").toAbsolutePath());
  }


  @Order(1)
  @Test void testDatabaseProcessor() throws SQLException {

    assertFalse(conn.isClosed());
    // jdbc connection type 4 no requirement to load driver.
    assertThatObject(conn).isExactlyInstanceOf(org.sqlite.jdbc4.JDBC4Connection.class);

  }

  @Order(2)
  @Test void testDropDDL() {

    assertAll("SQL command DROP does not throw Exception",
              () -> assertDoesNotThrow(() -> SQLCommands.BrooklynDDL.drop(JUnstructured.JUN_DROP.getAttribute())),
              () -> assertDoesNotThrow(() -> SQLCommands.BrooklynDDL.drop(JAnalytical.JAN_DROP.getAttribute())),
              () -> assertDoesNotThrow(() -> SQLCommands.BrooklynDDL.drop(JOperational.JOP_DROP.getAttribute())),
              () -> assertDoesNotThrow(() -> SQLCommands.BrooklynDDL.drop(JMaster.JMA_DROP.getAttribute())));

    assertThrows(SQLException.class, SQLCommands.BrooklynDDL::alter);
  }

  @Order(3)
  @Test void testCreateDDL() {

    assertAll("SQL command CREATE does not throw Exception",
              () -> assertDoesNotThrow(() -> SQLCommands.BrooklynDDL.create(JUnstructured.JUN_CREATE.getAttribute())),
              () -> assertDoesNotThrow(() -> SQLCommands.BrooklynDDL.create(JAnalytical.JAN_CREATE.getAttribute())),
              () -> assertDoesNotThrow(() -> SQLCommands.BrooklynDDL.create(JOperational.JOP_CREATE.getAttribute())),
              () -> assertDoesNotThrow(() -> SQLCommands.BrooklynDDL.create(JMaster.JMA_CREATE.getAttribute())));

    assertThrows(SQLException.class, SQLCommands.BrooklynDDL::alter);
  }

  @Order(4)
  @Test void testInsertDML() {

    String VALUE1 = "SQLException(just kidding)";
    String VALUE2 = "10:5:4:3";
    String VALUE3 = "file://some/parent/directory";
    String ATTR_2 = "[0-9A-I]";
    String ATTR_3 = "(~)+";
    String ATTR_4 = "png";

    assertAll("SQL command INSERT does not throw Exception",
              () -> assertDoesNotThrow(() -> SQLCommands.BrooklynDML.insert(JUnstructured.values(), DataServices.JUNSTRUCTURED, VALUE1)),
              () -> assertDoesNotThrow(() -> SQLCommands.BrooklynDML.insert(JAnalytical.values(), DataServices.JANALYTICAL, VALUE2)),
              () -> assertDoesNotThrow(() -> SQLCommands.BrooklynDML.insert(JOperational.values(), DataServices.JOPERATIONAL, VALUE3)),
              () -> assertDoesNotThrow(() -> SQLCommands.BrooklynDML.insert(JMaster.values(), DataServices.JMASTER, ATTR_2, ATTR_3, ATTR_4)));

    assertAll("BrooklynDML does throw SQLException on incorrect DataServices parameter",
              () -> assertThrows(SQLException.class, () -> SQLCommands.BrooklynDML.insert(JMaster.values(), DataServices.JMASTER, ATTR_2)),
              () -> assertThrows(SQLException.class, () -> SQLCommands.BrooklynDML.insert(JOperational.values(), DataServices.JOPERATIONAL, VALUE1, VALUE2, VALUE3)));
  }

  @Order(5)
  @Test void testSelectDQL() throws SQLException {

    int PK_1 = 1;

    assertAll("SQL command SELECT does not throw Exception",
        () -> assertDoesNotThrow(() -> SQLCommands.BrooklynDQL.select(JUnstructured.values(), DataServices.JUNSTRUCTURED, PK_1)),
        () -> assertDoesNotThrow(() -> SQLCommands.BrooklynDQL.select(JAnalytical.values(), DataServices.JANALYTICAL, PK_1)),
        () -> assertDoesNotThrow(() -> SQLCommands.BrooklynDQL.select(JOperational.values(), DataServices.JOPERATIONAL, PK_1)),
        () -> assertDoesNotThrow(() -> SQLCommands.BrooklynDQL.select(JMaster.values(), DataServices.JMASTER, PK_1)));

    List<List<Dto>> datastore = List.of(SQLCommands.BrooklynDQL.select(JUnstructured.values(), DataServices.JUNSTRUCTURED, PK_1),
                                        SQLCommands.BrooklynDQL.select(JAnalytical.values(), DataServices.JANALYTICAL, PK_1),
                                        SQLCommands.BrooklynDQL.select(JOperational.values(), DataServices.JOPERATIONAL, PK_1),
                                        SQLCommands.BrooklynDQL.select(JMaster.values(), DataServices.JMASTER, PK_1));

    assertThat(datastore.get(0).get(0).toString())
                        .startsWith("JUnstructuredDTO")
                        .contains("jun_id=1")
                        .contains("jun_log=SQLException(just kidding)");

    assertThat(datastore.get(1).get(0).toString())
                        .startsWith("JAnalyticalDTO")
                        .contains("jan_id=1")
                        .contains("jan_stats=10:5:4:3");

    assertThat(datastore.get(2).get(0).toString())
                        .startsWith("JOperationalDTO")
                        .contains("jop_id=1")
                        .contains("jop_uri=file://some/parent/directory");

    assertThat(datastore.get(3).get(0).toString())
                        .startsWith("JMasterDTO")
                        .contains("jma_id=1")
                        .contains("jma_regex=[0-9A-I]")
                        .contains("jma_dedupe=(~)+")
                        .contains("jma_ext=png");
  }

  @Order(6)
  @Test void testDeleteDML() throws SQLException {

    assertThrows(SQLException.class, SQLCommands.BrooklynDML::delete);

    assertFalse(conn.isClosed());

    // TEST: SQLCommandsTest Brooklyn::Schema,MetaData
    DatabaseMetaData meta = conn.getMetaData();
    var driver = meta.getDriverName();
    var schema = meta.getSchemas();
    var catalog = meta.getCatalogs();
    var client = meta.getMaxConnections();

    logger.info("Driver: {}", driver);
    logger.info("Schema: {}", schema);
    logger.info("Catalog: {}", catalog);
    logger.info("Client: {}", client);
  }

  @Order(7)
  @Test void testUpdateDML() throws SQLException {

    String VALUE1 = "SQLException(been there)";
    String VALUE2 = "10:10:10:10";
    String VALUE3 = "file://another/parent/directory";
    int PK_1 = 1;
    String ATTR_2 = "[1-5A-Ij-r]";
    String ATTR_3 = "(_)+";
    String ATTR_4 = "pdf";

    assertAll("SQL command UPDATE does not throw Exception",
        () -> assertDoesNotThrow(() -> SQLCommands.BrooklynDML.update(JUnstructured.values(), PK_1, VALUE1)),
        () -> assertDoesNotThrow(() -> SQLCommands.BrooklynDML.update(JAnalytical.values(), PK_1, VALUE2)),
        () -> assertDoesNotThrow(() -> SQLCommands.BrooklynDML.update(JOperational.values(), PK_1, VALUE3)),
        () -> assertDoesNotThrow(() -> SQLCommands.BrooklynDML.update(JMaster.values(), 4, PK_1, ATTR_2)),
        () -> assertDoesNotThrow(() -> SQLCommands.BrooklynDML.update(JMaster.values(), 5, PK_1, ATTR_3)),
        () -> assertDoesNotThrow(() -> SQLCommands.BrooklynDML.update(JMaster.values(), 6, PK_1, ATTR_4)));

    assertAll("SQL command UPDATE equals SELECT statement",
        () -> assertThat(SQLCommands.BrooklynDQL.select(JUnstructured.values(), DataServices.JUNSTRUCTURED, PK_1).get(0).toString()).contains(VALUE1),
        () -> assertThat(SQLCommands.BrooklynDQL.select(JAnalytical.values(), DataServices.JANALYTICAL, PK_1).get(0).toString()).contains(VALUE2),
        () -> assertThat(SQLCommands.BrooklynDQL.select(JOperational.values(), DataServices.JOPERATIONAL, PK_1).get(0).toString()).contains(VALUE3),
        () -> assertThat(SQLCommands.BrooklynDQL.select(JMaster.values(), DataServices.JMASTER, PK_1).get(0).toString()).contains(ATTR_2),
        () -> assertThat(SQLCommands.BrooklynDQL.select(JMaster.values(), DataServices.JMASTER, PK_1).get(0).toString()).contains(ATTR_3),
        () -> assertThat(SQLCommands.BrooklynDQL.select(JMaster.values(), DataServices.JMASTER, PK_1).get(0).toString()).contains(ATTR_4));
  }

  @Order(8)
  @Test void testDisconnectDDL() {

    assertDoesNotThrow(SubClassTCL::disconnect);
  }

  /*
  @Order(8)
  @Test void testRollback() throws SQLException {

    // NOTE: Savepoint, throw an error to rollback from, instantiate savepoint post ps.execute
    private boolean transaction(PreparedStatement ps, Savepoint sp) throws SQLException {
      try { throw new SQLException("test case exception"); }
      catch (SQLException sqle) { rollback().accept(connect(), sp); throw sqle; }
      finally { ps.clearParameters(); }
    }

    SubClassTCL tcl = new SubClassTCL();

    Supplier<Savepoint> sp = () -> tcl.savepoint().apply(conn);

    assertDoesNotThrow(sp::get);
    assertDoesNotThrow(() -> tcl.rollback().accept(conn, sp.get()));
  }
  */
}


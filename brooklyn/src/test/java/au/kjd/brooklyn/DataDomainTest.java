package au.kjd.brooklyn;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DataDomainTest {

  @Test
  void testJUnstructured() {
    assertEquals("jun_id", JUnstructured.JUN_ID.getAttribute());
    assertEquals("jun_log", JUnstructured.JUN_LOG.getAttribute());
    assertEquals(7, JUnstructured.values().length);
    assertEquals(0, JUnstructured.values()[0].ordinal());
    assertEquals(1, JUnstructured.values()[1].ordinal());
  }

  @Test
  void testJAnalytical() {
    assertEquals("jan_id", JAnalytical.JAN_ID.getAttribute());
    assertEquals("jan_stats", JAnalytical.JAN_STATS.getAttribute());
    assertEquals(7, JAnalytical.values().length);
    assertEquals(0, JAnalytical.values()[0].ordinal());
    assertEquals(1, JAnalytical.values()[1].ordinal());
  }

  @Test
  void testJOperational() {
    assertEquals("jop_id", JOperational.JOP_ID.getAttribute());
    assertEquals("jop_uri", JOperational.JOP_URI.getAttribute());
    assertEquals(7, JOperational.values().length);
    assertEquals(0, JOperational.values()[0].ordinal());
    assertEquals(1, JOperational.values()[1].ordinal());
  }

  @Test
  void testJMaster() {
    assertEquals("jma_id", JMaster.JMA_ID.getAttribute());
    assertEquals("jma_regex", JMaster.JMA_REGEX.getAttribute());
    assertEquals("jma_dedupe", JMaster.JMA_DEDUPE.getAttribute());
    assertEquals("jma_ext", JMaster.JMA_EXT.getAttribute());
    assertEquals(11, JMaster.values().length);
    assertEquals(0, JMaster.values()[0].ordinal());
    assertEquals(1, JMaster.values()[1].ordinal());
    assertEquals(2, JMaster.values()[2].ordinal());
    assertEquals(3, JMaster.values()[3].ordinal());
  }
}

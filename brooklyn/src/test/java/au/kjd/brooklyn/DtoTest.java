package au.kjd.brooklyn;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.assertj.core.api.Assertions.*;

class DtoTest {

  @Test
  void testUnstructuredDTO() {
    String str = """
        JUnstructuredDTO{
          jun_id='1',
          jun_log='SQLException("it hurts me too")'
        }
        """;

    JUnstructuredDTO dto = new JUnstructuredDTO(1, "SQLException(\"it hurts me too\")");

    assertEquals(1, dto.junId());
    assertTrue(dto.junLog().contains("SQLException"));
    assertThat(dto.toString()).contains("jun_id", "jun_log");
  }

  @Test
  void testJAnalyticalDTO() {
    String str = """
          JAnalyticalDTO{
            jan_id='1',
            jan_stats='10:10:10:10'
          }
          """;

    JAnalyticalDTO dto = new JAnalyticalDTO(1, "10:10:10:10");

    assertEquals(1, dto.janId());
    assertThat(dto.janStats()).contains(":10:");
  }

 @Test
  void testJOperationalDTO() {
    String str = """
          JOperationalDTO{
            jop_id='1',
            jop_uri='file://a/parent/directory'
          }
          """;

    JOperationalDTO dto = new JOperationalDTO(1, "file://a/parent/directory");

    assertEquals(1, dto.jopId());
    assertThat(dto.jopUri()).matches("file://a/parent/directory");
    assertThat(dto.toString()).contains("jop_id", "jop_uri");
  }

  @Test
  void testJMasterDTO() {
    String str = """
      JMasterDTO{
        jma_id='1',
        jma_regex='[^0-9A-Z_a-z]',
        jma_dedupe='(_)+',
        jma_ext='all'
      }
      """;

    JMasterDTO dto = new JMasterDTO(1, "[^0-9A-Z_a-z]", "(_)+", "all");

    assertEquals(1, dto.jmaId());
    assertThat(dto.jmaRegex()).contains("^", "0-9", "A-Z", "_", "a-z");
    assertThat(dto.jmaDedupe()).contains("_", "+");
    assertThat(dto.jmaExt()).matches("all");
  }
}

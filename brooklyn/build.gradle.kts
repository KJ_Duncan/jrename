plugins { id("jrename.java-library-conventions") }

dependencies {
  constraints {
    implementation("org.xerial:sqlite-jdbc:3.36.0.3")

    testImplementation("org.assertj:assertj-db:2.0.2")
  }

  dependencies {
    implementation("org.xerial:sqlite-jdbc")

    testImplementation("org.assertj:assertj-db")
  }
}

plugins { java }

// jrename.${project}.au.kjd.${project}
group = "au.kjd.jrename"
version = "1.0.0"

// val moduleName by extra("jrename")
// val javaHome: String = System.getProperty("java.home")

repositories { mavenCentral() }

/* junit5/intellij: https://junit.org/junit5/docs/current/user-guide/#running-tests-ide-intellij-idea
 * junit5/console:  https://repo1.maven.org/maven2/org/junit/platform/junit-platform-console-standalone/
 * junit5/intellij: https://junit.org/junit5/docs/current/user-guide/#running-tests-ide-intellij-idea
 *                  testRuntimeOnly("org.junit.platform:junit-platform-launcher:1.7.2")
 * sqlite-jdbc: https://github.com/xerial/sqlite-jdbc
 */
dependencies {
  constraints {
    implementation("org.apache.logging.log4j:log4j-api:2.14.1")
    implementation("org.owasp:security-logging-common:1.1.6")
    implementation("org.owasp:security-logging-log4j:1.1.6")

    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.1")
    testImplementation("org.junit.jupiter:junit-jupiter-params:5.8.1")
    testImplementation("org.assertj:assertj-core:3.21.0")

    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.8.1")
  }

  dependencies {
    implementation("org.apache.logging.log4j:log4j-api")
    implementation("org.owasp:security-logging-common")
    implementation("org.owasp:security-logging-log4j")

    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testImplementation("org.junit.jupiter:junit-jupiter-params")
    testImplementation("org.assertj:assertj-core")

    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
  }
}

/* testLogging events
   "standard_out",
   "standard_error"

   "-Xlint:cast,rawtypes,static,unchecked,preview"

   Enabling Java preview features; compulation, test, and runtime
   <https://.../gradle-7.2/docs/userguide/building_java_projects.html#sec:feature_preview>
 */
tasks.withType<JavaCompile> { options.compilerArgs = listOf("--release", "17") }
tasks.withType<Test>        { jvmArgs("--enable-preview", "-enableassertions") }
// tasks.withType<JavaExec>    { jvmArgs("--enable-preview") }

tasks {
  /* https://docs.gradle.org/current/userguide/java_library_plugin.html#sec:java_library_modular
   * Gradle - Building Modules for the Java Module System
   *        - Java Module System support is an incubating feature
   */
  compileJava {
    options.javaModuleMainClass.set("au.kjd.gui.JRenameGUI")
    options.javaModuleVersion.set(provider { project.version as String })
    java.modularity.inferModulePath.set(true)
  }

  test {
    useJUnitPlatform { excludeTags("Playground", "Undetermined", "Broken") }
    failFast = true
    testLogging.showStandardStreams = true
    testLogging {
      events(
        "passed",
        "failed",
        "skipped"
      )
    }
  }

  jar {
    enabled = true

    from(sourceSets.main.get().output)
    exclude { details: FileTreeElement ->
      details.file.name.endsWith(".png")  ||
      details.file.name.endsWith(".jpg")  ||
      details.file.name.endsWith(".puml") ||
      details.file.name.endsWith(".cmapx")
    }

    /* need to rerun:
     *  FIXME: $ jar --update --file build/libs/rename.java.jar --main-class au.kjd.Rename
     */
    manifest {
      attributes(
        "Manifest-Version" to "1.0",
        "Created-By" to "Kirk Duncan",
        "Name" to "au/kjd/jrename/",
        "Sealed" to "true",
        "Main-Class" to "au.kjd.gui.JRenameGUI",
        "Specification-Title" to "Rename file utility",
        "Specification-Version" to "${project.version}"
      )
    }
  }

  /* ilya-g 2020, build.gradle.kts, kotlin-jlink-examples, gradle, viewed 01 March 2021,
   *              https://github.com/ilya-g/kotlin-jlink-examples/blob/master/gradle/app/build.gradle.kts
   *
   * TODO: gradlew help --task jlink, options for international locales -Dprop=value
  register<Exec>("jlink") {
    description = "Build jrename module jar with an optimised custom runtime image"
    val outputDir by extra("$buildDir/jrt-jrename")
    inputs.files(configurations.runtimeClasspath)
    inputs.files(jar)
    outputs.dir(outputDir)
    doFirst {
      val modulePath = files(jar) + configurations.runtimeClasspath.get()
      logger.lifecycle(modulePath.joinToString("\n", "jlink module path:\n"))
      logger.lifecycle("\njlink custom runtime:\n ${outputDir}\n")
      delete(outputDir)
      commandLine("$javaHome/bin/jlink",
                  "--module-path",
                  listOf("$javaHome/jmods/", modulePath.asPath).joinToString(File.pathSeparator),
                  "--add-modules", moduleName,
                  "--output", outputDir
      )
    }
  }
   */
}

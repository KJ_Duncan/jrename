plugins {
  /* shared build configuration between library and application projects */
  id("jrename.java-common-conventions")

  /* API and implementation separation */
  `java-library`
}

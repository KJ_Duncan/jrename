plugins {
  /* shared build configuration between library and application projects */
  id("jrename.java-common-conventions")

  /* support for building a CLI applications in Java */
  application
}
// applicationDefaultJvmArgs = listOf("-Dproperty=value")
application {
  mainModule.set("au.kjd.gui")
  mainClass.set("au.kjd.gui.JRenameGUI")
}

/* brooklyn is setup as plugins { id("jrename.java-library-conventions") } a library not a api

   observation: reports unknown module when first run ./gradlew check --rerun-tasks,
                all subsequent call to ./gradlew check are without error

   brooklyn/src/main/java/module-info.java:20: warning: [module] module not found: jrename.au.kjd.jrename
     exports au.kjd.brooklyn to jrename.au.kjd.jrename;
                                                ^

   observation: intellij gradle check reports no errors and builds the brooklyn.jar

   there are two separate operations: gradle build project, java module system.
   the java module system imposes the accessibility restriction system (strong encapsulation)
   without an export and subsequent requires directive public is not public.

   java module directive `requires` gradle configuration `implementation`.
   java module directive `requires transitive` gradle configuration 'api'.

  dependencies {
    implementation(project(":brooklyn"))
    implementation(project(":jrename"))
  }
 */

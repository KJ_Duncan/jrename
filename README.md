As at 30 September 2021 - Not production ready, proof of concept for [pbe-credentials](https://bitbucket.org/KJ_Duncan/pbe-credentials.kt/src/master/).
  
```
Beastie Boys, 1986, No Sleep Till Brooklyn, Licensed To Ill, viewed 13 June 2021,
                    <https://open.spotify.com/track/5qxChyzKLEyoPJ5qGrdurN?si=2c0fc8309925402a>
```


----


#### PURPOSE


- Standardise file names within a directory.
    - Traversal through a large file hierarchy on a local system to a specified depth.
- Resource allocation and consumption aware and at the users discretion.
    - Interquartile range for summary statistics.
- Access control of system resources asks the _Who_ and _Where_ before the _How_.
    - Secure logging of information and error reporting
- The _How_ is in the requirements gathering stage.
    - A batch process
    - Low resources, low consumption, consistent behaviour (JMH, SQLite)
    - A micro jvm to do the job without interfering with my job (jlink).
- Pass on the knowledge to the user. The _Why_ and _How_.


----


#### Doing


- [ ] Exceptions
- [ ] Graphical User Interface
- [ ] Logging
- [ ] Test suite - for current progress see `chicken_scratchings.txt & src/test/java/au/kjd/*.java`.


#### Todo


- [ ] Benchmark
- [ ] Documentation
- [ ] Internationalisation
- [ ] Message Bus
- [ ] Module System
- [ ] Statistics


#### Done


- [x] Database


----


![](plantuml/jpg/Data-Dictionary_Brooklyn.jpg)


  


![](plantuml/png/brooklyn_database.png)


----

![](plantuml/png/brooklynDDL_sequence_diagram.png)

----

![](plantuml/png/brooklynDML_sequence_diagram.png)

----

![](plantuml/png/brooklynDQL_sequence_diagram.png)

----

![](plantuml/png/walker_sequence_diagram.png)

----

![](plantuml/jpg/jrename-gui.jpg)

----


```
$ ./gradlew test --rerun-tasks

$ xdg-open brooklyn/build/reports/tests/test/index.html
$ xdg-open jrename/build/reports/tests/test/index.html
$ xdg-open brooklyn/src/test/java/resources/test.log
$ xdg-open jrename/src/test/java/resources/test.log
```
  
![](plantuml/png/jrename-tests.png)


----


#### Development Tools


- AlMr 2021, kakhist, kakoune plugin, gitlab, viewed 10 July 2021, <https://gitlab.com/kstr0k/mru-files.kak/-/tree/master/kakhist>
- Canonical 2021, Ubuntu on WSL, Windows Subsystem for Linux (WSL), viewed 24 June 2021, <https://ubuntu.com/wsl>
- Duncan, K 2021, auditgradle.sh, Portable shell script to audit the Gradle Wrapper, viewed 24 June 2021, <https://bitbucket.org/KJ_Duncan/workspace/snippets/LpLedA/portable-shell-script-to-audit-the-gradle>
- Duncan, K 2021, grep-buffer.kak, TODO|FIXME|NOTES..., kakoune plugin, viewed 24 June 2021, <https://bitbucket.org/KJ_Duncan/workspace/snippets/KrGkBa>
- Duncan, K 2021, jreplace, replace.java, viewed 27 June 2021, <https://bitbucket.org/KJ_Duncan/replace.java/src/master/>
- Duncan, K 2021, kakoune-java, java se 16, kakoune plugin viewed 24 June 2021, <https://bitbucket.org/KJ_Duncan/kakoune-java.kak/src/master/>
- Duncan, K 2021, kakoune-plantuml, plantuml, kakoune plugin viewed 24 June 2021, <https://bitbucket.org/KJ_Duncan/kakoune-plantuml.kak/src/master/>
- Felice, J 2021, kak-ansi, ANSI-coloured text, kakoune plugin, viewed 10 July 2021, <https://github.com/eraserhd/kak-ansi>
- Git 2021, git, version control system, v2.32.0, viewed 07 July 2021, <https://git-scm.com/> 
- Gradle 2021, Software Build Tool, Gradle User Manual, viewed 24 June 2021, <https://docs.gradle.org/current/userguide/userguide.html>
- Jarun 2021, nnn, terminal file manager, jarun/nnn/github, viewed 29 June 2021, <https://github.com/jarun/nnn>
- JetBrains 2021, IntelliJ IDEA, Integrated Development Environment (IDE), viewed 24 June 2021, <https://www.jetbrains.com/help/idea/2021.1/discover-intellij-idea.html>
- Kakoune 2021, Terminal Modal Editor, mawww/kakoune, github, viewed 24 June 2021, <https://github.com/mawww/kakoune>
- Lenormand, F 2018, grepmenu.kak, kakoune plugin, github/lenormf/kakoune-extra, viewed 29 June 2021, <https://github.com/lenormf/kakoune-extra/blob/master/grepmenu.kak>
- Oracle 2020, Java Platform, Standard Edition & Java Development Kit, Version 16 API Specification, viewed 24 June 2021, <https://docs.oracle.com/en/java/javase/16/docs/api/index.html>
- PlantUML 2021, PlantUML, Generate UML diagram from textual description, github, viewed 24 June 2021, <https://plantuml.com/>
- Polar 2021, Polar, Read. Learn. Never Forget., integrated reading environment, viewed 24 June 2021, <https://getpolarized.io/>
- SQLite Consortium 2021, SQLite, version 3.36.0, viewed 25 June 2021, <https://www.sqlite.org/index.html>


----


#### Readings


- Begg C & Connolly, T 2005, Database systems: a practical approach to design, implementation, and management, fourth edition, Addison-Wesley, Boston, MA
- Deogun, D Johnsson, D.B & Sawano D 2019, Secure by Design, Manning Publications, viewed 25 June 2021, <https://www.manning.com/books/secure-by-design>
- Fusco, M Urma, G-R & Mycroft, A 2018, Modern Java in Action, Manning Publications, viewed 25 June 2021, <https://www.manning.com/books/modern-java-in-action>
- Henney, K & Parlog, N 2019, The Java Module System, Manning Publications, viewed 25 June 2021, <https://www.manning.com/books/the-java-module-system>
- Saumont, P-Y 2017, Functional Programming in Java, Manning Publications, viewed 08 October 2021, <https://www.manning.com/books/functional-programming-in-java> 
- Tudose, C 2020, JUnit in Action, Third Edition, Manning Publications, viewed 25 June 2021, <https://www.manning.com/books/junit-in-action-third-edition>


----


In the meantime or alternatively:  
```shell
# tail is new the head is stolen, thanks Adha
$ printf "%s" "$1" | sed -e 's/[^-.0-9A-Z_a-z]/_/g' -e 's/\(_\)\+/_/g' -e 's/\(_\.\)/./g' -e 's/\(_\)+$//g'

# even for an optimist do not use on any files you care about (reports/confidential/sensitive/work/school/mums/... files)
$ fd --type file --extension <ext> --exec-batch fixname.bash
$ sqlite3 -A --create --file singlefile.sqlar
 
jshell> '-' < '.' && '.' < '0' && '0' < '9' && '9' < 'A' && 'A' < 'Z' && 'Z' < '_' && '_' < 'a' && 'a' < 'z'
$1 ==> true

Benawi, A 2021, fixname, jarun/nnn/plugins/, viewed 30 June 2021,
                <https://github.com/jarun/nnn/blob/master/plugins/fixname>

Addiction, J 1990, Been Caught Stealing, Ritual De Lo Habitual, viewed 30 June 2021,
                   <https://open.spotify.com/track/4Qievb8Mqy0qxdLNVl02zt?si=e9afe0e70e3d4069>

```


----


```shell
# source adapted: Benawi Adha <https://github.com/jarun/nnn/blob/master/plugins/fixname>

# fixname.bash
#! /usr/bin/env bash

print_help() { printf "%s\n" "Usage: fd --type file --extension <ext> --exec-batch fixname.bash"; }
if [ $# -eq 0 ] ; then print_help; exit 1; fi

# --exec-batch: prompt=true
prompt=true
targets=()

cleanup() {
  printf "%s" "$1" | sed -e 's/[^-.0-9A-Z_a-z]/_/g' -e 's/\(_\)\+/_/g' -e 's/\(_\.\)/./g' -e 's/^\(_\)+//' -e 's/\(_\)+$//'
  #                           anyother character         deduplicate        prefix ext           prefix          suffix
}

func_args() {
  while [ $# -gt 0 ]; do
    targets+=( "$(basename "$1")" )
    shift
  done
}

if [ $# -gt 1 ]; then func_args "$@"; else targets=("$1"); fi

for i in "${targets[@]}"; do
  printf "%s -> %s\n" "$i" "$(cleanup "$i")";
done

if $prompt; then
  echo
  printf "Proceed [y|N]? "
  read -r input
  case "$input" in
    n|N|'')
      echo "Canceled"
      exit 1
      ;;
    *)
      ;;
  esac
fi

# prefix a maximum number of two '_' to duplicate file names
# _How_to_access_MySQL_Workbench.docx
# __How_to_access_MySQL_Workbench.docx
for i in "${targets[@]}"; do
  if [ "$i" != "$(cleanup "$i")" ]; then
    tmp=''
    if [ -e "$(cleanup "$i")" ]; then
        tmp='_'
    fi
    mv "$i" "$tmp$(cleanup "$i")";
  fi
done
```


![](plantuml/jpg/nnn-fixname.jpg)


----


#### As at 03 July 2021 - Lessons learnt 


![](plantuml/jpg/jrename-buildSrc.jpg)

As the project will continue to grow in size on a system housing:  
  
AMD FX(tm)-6100 Six-Core Processor 3.30 GHz (2011)  
RAM: 8.00 GB  
  
Development of the project seems out of reach for the system. Hence, rolling back to commit `1686ba5` 2021-06-23 pre-gradle multi-project build; defensive push aptly renamed to risk transference.  


Gradle buildSrc authors first multi-project build:  


- Gradle uses ':' as an aliase for path separator '/'.
- module-info.java low barrier to entry.
- Git commit --the-blob is a walk of shame. An appropriate git branch and commit strategy is necessary.
- settings.gradle.kts lower barrier to entry.
- 8GB RAM runs at minimum 84% of total in this build on idle (intellij/gradle/wsl.exe/kakoune/nnn).
- 5 packages too ambitious for a first attempt. Considering a 2 package showdown (data domain to application layer).
- Intellij an enterprise grade IDE does help fixing multi-project holes (disclaimer: university subscription plan).
- Low resources, low consumption kakoune and nnn for the win.
- ServiceLoader refer to list of references for details (chicken_scratchings.txt)


Break it to make it, thanks gradle and java enjoyed the multi-project build challenge coupled with great supporting documentation.  
  
The world before Git: I don't know and I don't want to know. What a great tool.  


----




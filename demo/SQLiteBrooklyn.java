import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.lang.System;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

import static java.lang.String.format;
import static java.lang.String.valueOf;
import static java.lang.System.exit;

final class SQLiteBrooklyn {

  /* unstructured data will have high transaction costs due to parse and transform logs, need to decouple.
   * data warehouse hold analytical data processing with medium/high frequency.
   * operational data high frequency transactions on each directory change uri update.
   * master data low/medium/high frequency depending on the file extension(s) selected by the user.
   * metadata is data about the data can use xml-catalog or sqlite3-howdoi.md.
   *
   * relations PK,FK application layer will do this job for us as some sort of message/event-bus (publish/subscribe/serviceLoader)? still undetermined.
   *
   *   1 database  4 tables <-- yes, made sense when modelled than the latter.
   *   4 databases 1 table
   *
   * 2 Directions:
   *    we have the same number of tables regardless of direction (table/row locking on data frequency is the concern)
   *    3 out 4 tables share the same shape
   *    if we generalise we reduce code bloat and clarity and vice-versa
   *
   *    low resources, low consumption, consistent results
   *
   *    pre-condition: everything that could go wrong, would go wrong, has gone wrong
   *    post-condition: monkey magic
   *
   * $ javac --release 17 SQLiteBrooklyn.java
   *
   * $ java -classpath .:$HOME/<path-to-jar>/sqlite-jdbc-3.36.0.3.jar SQLiteBrooklyn
   * > "help message"
   *
   * $ java -classpath .:$HOME/<path-to-jar>/sqlite-jdbc-3.36.0.3.jar SQLiteBrooklyn run
   * > stdout
   *
   * $ sqlite3 master.db
   * sqlite>.mode box
   * sqlite> select * from jmaster;
   *  ┌────────┬────────────┬────────────┬─────────┐
   *  │ jma_id │ jma_regex  │ jma_dedupe │ jma_ext │
   *  ├────────┼────────────┼────────────┼─────────┤
   *  │ 1      │ [^A-Z]     │ (_)+       │ all     │
   *  │ 2      │ [^0-9-a-z] │ (-)+       │ mp4     │
   *  │ 3      │ [^0-5~a-e] │ (~)+       │ jpg     │
   *  └────────┴────────────┴────────────┴─────────┘
   * sqlite>.exit
   *
   * kakoune kangaroo: %s^//<ret>
   */
// ---------------------------------------------------------------------------------------------- \\
  private static final String JUNSTRUCTURED = "junstructured";
  private static final String JUN_ID        = "jun_id";
  private static final String JUN_LOG       = "jun_log";

  private static final String JANALYTICAL = "janalytical";
  private static final String JAN_ID      = "jan_id";
  private static final String JAN_STATS   = "jan_stats";

  private static final String JOPERATIONAL = "joperational";
  private static final String JOP_ID       = "jop_id";
  private static final String JOP_URI      = "jop_uri";

  private static final String JMASTER    = "jmaster";
  private static final String JMA_ID     = "jma_id";
  private static final String JMA_REGEX  = "jma_regex";
  private static final String JMA_DEDUPE = "jma_dedupe";
  private static final String JMA_EXT    = "jma_ext";
// ---------------------------------------------------------------------------------------------- \\
  private static final String DROP_TABLE_JUNSTRUCTURED = "DROP TABLE IF EXISTS junstructured";
  private static final String DROP_TABLE_JANALYTICAL   = "DROP TABLE IF EXISTS janalytical";
  private static final String DROP_TABLE_JOPERATIONAL  = "DROP TABLE IF EXISTS joperational";
  private static final String DROP_TABLE_JMASTER       = "DROP TABLE IF EXISTS jmaster";
// ---------------------------------------------------------------------------------------------- \\
  private static final String INSERT_INTO_JUNSTRUCTURED = "INSERT INTO junstructured ( jun_log   ) VALUES ( ? )";
  private static final String INSERT_INTO_JANALYTICAL   = "INSERT INTO janalytical   ( jan_stats ) VALUES ( ? )";
  private static final String INSERT_INTO_JOPERATIONAL  = "INSERT INTO joperational  ( jop_uri   ) VALUES ( ? )";
  private static final String INSERT_INTO_JMASTER       = "INSERT INTO jmaster ( jma_regex, jma_dedupe, jma_ext ) VALUES ( ?, ?, ? )";

  private static final String SELECT_ROW_JUNSTRUCTURED = format("SELECT %s, %s FROM %s WHERE %s = ?", JUN_ID, JUN_LOG, JUNSTRUCTURED, JUN_ID);
  private static final String SELECT_ROW_JANALYTICAL   = format("SELECT %s, %s FROM %s WHERE %s = ?", JAN_ID, JAN_STATS, JANALYTICAL, JAN_ID);
  private static final String SELECT_ROW_JOPERATIONAL  = format("SELECT %s, %s FROM %s WHERE %s = ?", JOP_ID, JOP_URI, JOPERATIONAL, JOP_ID);
  private static final String SELECT_ROW_JMASTER       = format("SELECT %s, %s, %s, %s FROM %s WHERE %s = ?", JMA_ID, JMA_REGEX, JMA_DEDUPE, JMA_EXT, JMASTER, JMA_ID);

  private static final String SELECT_TABLE_JUNSTRUCTURED = format("SELECT * FROM %s", JUNSTRUCTURED);
  private static final String SELECT_TABLE_JANALYTICAL   = format("SELECT * FROM %s", JANALYTICAL);
  private static final String SELECT_TABLE_JOPERATIONAL  = format("SELECT * FROM %s", JOPERATIONAL);
  private static final String SELECT_TABLE_JMASTER       = format("SELECT * FROM %s", JMASTER);
// ---------------------------------------------------------------------------------------------- \\
  private static final String CREATE_TABLE_JUNSTRUCTURED = """
      CREATE TABLE junstructured (
        jun_id  INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        jun_log TEXT    NOT NULL DEFAULT 'All good',

        CONSTRAINT jun_ck1 CHECK ( 0 < LENGTH( jun_log ) )
      );
      """;

  private static final String CREATE_TABLE_JANALYTICAL = """
      CREATE TABLE janalytical (
        jan_id    INTEGER      NOT NULL PRIMARY KEY AUTOINCREMENT,
        jan_stats VARCHAR(255) NOT NULL DEFAULT 'range:mean:median:mode',

        CONSTRAINT jan_ck1 CHECK ( 0 < LENGTH( jan_stats ) )
      );
      """;

  private static final String CREATE_TABLE_JOPERATIONAL = """
      CREATE TABLE joperational (
        jop_id  INTEGER      NOT NULL PRIMARY KEY AUTOINCREMENT,
        jop_uri VARCHAR(255) NOT NULL DEFAULT 'success',

        CONSTRAINT jop_ck1 CHECK ( 0 < LENGTH( jop_uri ) )
      );
      """;

  private static final String CREATE_TABLE_JMASTER = """
      CREATE TABLE jmaster (
        jma_id     INTEGER      NOT NULL PRIMARY KEY AUTOINCREMENT,
        jma_regex  VARCHAR(255) NOT NULL DEFAULT '[^0-9A-Z_a-z]',
        jma_dedupe VARCHAR(55)  NOT NULL DEFAULT '(_)+',
        jma_ext    VARCHAR(6)   NOT NULL DEFAULT '*.*',

        CONSTRAINT jma_ck1 CHECK ( 0 < LENGTH( jma_regex ) ),
        CONSTRAINT jma_ck2 CHECK ( 0 < LENGTH( jma_dedupe ) ),
        CONSTRAINT jma_ck3 CHECK ( 0 < LENGTH( jma_ext ) )
      );
      """;
// ---------------------------------------------------------------------------------------------- \\
  private static final Object[][] INIT_DATA_JUNSTRUCTURED = {
    { "ExceptionalFunction(ace.getMessage())" },
    { "ioe.printStackTrace()" },
    { "Throwable.getCause()" }};

  private static final Object[][] INIT_DATA_JANALYTICAL = {
    { "5:4:3:2" },
    { "15:3:5:12" },
    { "3:2:1:0" }};

  private static final Object[][] INIT_DATA_JOPERATIONAL = {
    { "file://path/to/parent/directory" },
    { "SUCCESS" },
    { "file://path/to/parent/directory/" }};

  private static final Object[][] INIT_DATA_JMASTER = {
    { "[^A-Z]", "(_)+", "all" },
    { "[^0-9-a-z]", "(-)+", "mp4" },
    { "[^0-5~a-e]", "(~)+", "jpg" }};


// ---------------------------------------------------------------------------------------------- \\
  private enum Database {
    UNSTRUCTURED_DB("jdbc:sqlite:unstructured.db"),
    ANALYTICAL_DB("jdbc:sqlite:analytical.db"),
    OPERATIONAL_DB("jdbc:sqlite:operational.db"),
    MASTER_DB("jdbc:sqlite:master.db");

    private String url;
    private Database(String url) { this.url = url; }
    String getUrl() { return this.url; }
  }
// ---------------------------------------------------------------------------------------------- \\
  private sealed interface DATA_DOMAIN permits JUnstructured, JAnalytical, JOperational, JMaster { }

  private sealed interface DTO permits JUnstructuredDTO, JAnalyticalDTO, JOperationalDTO, JMasterDTO { }
// ---------------------------------------------------------------------------------------------- \\
  /* ordinal values 0,1,2,3 */
  private enum JUnstructured implements DATA_DOMAIN { // ordinal value 0,1
    JUN_ID("jun_id"), JUN_LOG("jun_log");

    private String attribute;
    private JUnstructured(String attr) { this.attribute = attr; }
    String getAttribute() { return attribute; }
  }

  private enum JAnalytical implements DATA_DOMAIN {
    JAN_ID("jan_id"), JAN_STATS("jan_stats");

    private String attribute;
    private JAnalytical(String attr) { this.attribute = attr; }
    String getAttribute() { return attribute; }
  }

  private enum JOperational implements DATA_DOMAIN {
    JOP_ID("jop_id"), JOP_URI("jop_uri");

    private String attribute;
    private JOperational(String attr) { this.attribute = attr; }
    String getAttribute() { return attribute; }
  }

  private enum JMaster implements DATA_DOMAIN {
    JMA_ID("jma_id"), JMA_REGEX("jma_regex"), JMA_DEDUPE("jma_dedupe"), JMA_EXT("jma_ext");

    private String attribute;
    private JMaster(String attr) { this.attribute = attr; }
    String getAttribute() { return attribute; }
  }
// ---------------------------------------------------------------------------------------------- \\
  /* Unstructured data */
  private final record JUnstructuredDTO(int junId, String junLog) implements DTO {
    @Override public String toString() {
      return """
        JUnstructuredDTO{
          jun_id='%s',
          jun_log='%s'
        }""".formatted(junId, junLog);
    }
  }

  /* Data warehouse (OLAP) */
  private final record JAnalyticalDTO(int jwaId, String jwaStats) implements DTO {
    @Override public String toString() {
      return """
        JAnalyticalDTO{
          jan_id='%s',
          jan_stats='%s'
        }""".formatted(jwaId, jwaStats);
    }
  }

  /* Operational data (OLTP) */
  private final record JOperationalDTO(int jopId, String jopUri) implements DTO {
    @Override public String toString() {
      return """
        JOperationalDTO{
          jop_id='%s',
          jop_uri='%s'
        }""".formatted(jopId, jopUri);
    }
  }

  /* Master data */
  private final record JMasterDTO(int jmaId, String jmaRegex, String jmaDedupe, String jmaExt) implements DTO {
    @Override public String toString() {
      return """
        JMasterDTO{
          jma_id='%s',
          jma_regex='%s',
          jma_dedupe='%s',
          jma_ext='%s'
        }""".formatted(jmaId, jmaRegex, jmaDedupe, jmaExt);
    }
  }
// ---------------------------------------------------------------------------------------------- \\
  /* a wrapper function to perform an abstraction at the call site (noise reduction) */
  private static String getEntity(DATA_DOMAIN table) {
    if (table instanceof JUnstructured jun)     { return jun.attribute; }
    else if (table instanceof JAnalytical jan)  { return jan.attribute; }
    else if (table instanceof JOperational jop) { return jop.attribute; }
    else if (table instanceof JMaster jma)      { return jma.attribute; }
    else throw new IllegalStateException("No data in the domain");
  }

  /* public static ... factory entry point */
  private static List<DTO> getRowDTO(Database db, DATA_DOMAIN[] attrs, int PK) {
    return switch (db) {
      case UNSTRUCTURED_DB -> viewRowUnstructured(attrs, PK);
      case ANALYTICAL_DB   -> viewRowWarehouse(attrs, PK);
      case OPERATIONAL_DB  -> viewRowOperational(attrs, PK);
      case MASTER_DB       -> viewRowMaster(attrs, PK);
    };
  }

  /* public static ... factory entry point */
  private static List<DTO> getTableDTO(Database db, DATA_DOMAIN[] attrs) {
    return switch (db) {
      case UNSTRUCTURED_DB -> viewTableUnstructured(attrs);
      case ANALYTICAL_DB   -> viewTableWarehouse(attrs);
      case OPERATIONAL_DB  -> viewTableOperational(attrs);
      case MASTER_DB       -> viewTableMaster(attrs);
    };
  }
// ---------------------------------------------------------------------------------------------- \\
  private static Connection connect(Database db) {
    Connection conn = null;

    try { conn = DriverManager.getConnection(db.url); }
    catch (SQLException sqlex) { System.err.println(sqlex.getMessage()); }
    return conn;
  }
// ---------------------------------------------------------------------------------------------- \\
  private static void create(final Database db, final String sql) { execute(db, sql); }

  private static void drop(final Database db, final String sql) { execute(db, sql); }
// ---------------------------------------------------------------------------------------------- \\
  private static void execute(final Database db, final String sql) {

    try(final Connection conn = connect(db);
        PreparedStatement ps = conn.prepareStatement(sql)) {
        conn.setAutoCommit(false);

        ps.executeUpdate();

        conn.commit(); }
   catch (SQLException sqlex) { System.err.println(sqlex.getMessage()); }
  }
// ---------------------------------------------------------------------------------------------- \\
  private static void insertJUnstructured() {

    for (Object[] datum:
         INIT_DATA_JUNSTRUCTURED) {

      try (final Connection conn = connect(Database.UNSTRUCTURED_DB);
           PreparedStatement ps = conn.prepareStatement(INSERT_INTO_JUNSTRUCTURED)) {
           conn.setAutoCommit(false);

           ps.setString(1, valueOf(datum[0]));
           ps.executeUpdate();

           conn.commit(); }
      catch (SQLException sqlex) { System.err.println(sqlex.getMessage()); }
    }
  }
// ---------------------------------------------------------------------------------------------- \\
  private static void insertJAnalytical() {

    for (Object[] datum:
        INIT_DATA_JANALYTICAL) {

      try (final Connection conn = connect(Database.ANALYTICAL_DB);
           PreparedStatement ps = conn.prepareStatement(INSERT_INTO_JANALYTICAL)) {
           conn.setAutoCommit(false);

           ps.setString(1, valueOf(datum[0]));
           ps.executeUpdate();

           conn.commit(); }
      catch (SQLException sqlex) { System.err.println(sqlex.getMessage()); }
    }
  }
// ---------------------------------------------------------------------------------------------- \\
  private static void insertJOperational() {

    for (Object[] datum:
         INIT_DATA_JOPERATIONAL) {

      try (final Connection conn = connect(Database.OPERATIONAL_DB);
           PreparedStatement ps = conn.prepareStatement(INSERT_INTO_JOPERATIONAL)) {
           conn.setAutoCommit(false);

           ps.setString(1, valueOf(datum[0]));
           ps.executeUpdate();

           conn.commit(); }
      catch (SQLException sqlex) { System.err.println(sqlex.getMessage()); }
    }
  }
// ---------------------------------------------------------------------------------------------- \\
  private static void insertJMaster() {

    for (Object[] datum:
         INIT_DATA_JMASTER) {

      try (final Connection conn = connect(Database.MASTER_DB);
           PreparedStatement ps = conn.prepareStatement(INSERT_INTO_JMASTER)) {
           conn.setAutoCommit(false);

           ps.setString(1, valueOf(datum[0]));
           ps.setString(2, valueOf(datum[1]));
           ps.setString(3, valueOf(datum[2]));
           ps.executeUpdate();

           conn.commit(); }
      catch (SQLException sqlex) { System.err.println(sqlex.getMessage()); }
    }
  }
// ---------------------------------------------------------------------------------------------- \\
  /* viewRow and viewTable ResultSet: 3 of 4 tables share the same shape[] enum.values()
   *       return DTO is: JUnstructuredDTO, JAnalyticalDTO, ...
   */
  private static List<DTO> viewRowUnstructured(DATA_DOMAIN[] attr, int PK) {

    final List<DTO> row = new ArrayList<>();

    try (final Connection conn = connect(Database.UNSTRUCTURED_DB);
         final PreparedStatement ps = conn.prepareStatement(SELECT_ROW_JUNSTRUCTURED)) {
         conn.setAutoCommit(false);

         ps.setInt(1, PK);
         ResultSet rs = ps.executeQuery();

         while (rs.next()) {
           row.add(new JUnstructuredDTO(rs.getInt(getEntity(attr[0])),
                                        rs.getString(getEntity(attr[1])))); }

         conn.commit(); }
    catch (SQLException sqlex) { System.err.println(sqlex.getMessage()); }

    return row;
  }
// ---------------------------------------------------------------------------------------------- \\
  private static List<DTO> viewRowWarehouse(DATA_DOMAIN[] attr, int PK) {

    final List<DTO> row = new ArrayList<>();

    try (final Connection conn = connect(Database.ANALYTICAL_DB);
         final PreparedStatement ps = conn.prepareStatement(SELECT_ROW_JANALYTICAL)) {
         conn.setAutoCommit(false);

         ps.setInt(1, PK);
         ResultSet rs = ps.executeQuery();

         while (rs.next()) {
           row.add(new JAnalyticalDTO(rs.getInt(getEntity(attr[0])),
                                     rs.getString(getEntity(attr[1])))); }

         conn.commit(); }
    catch (SQLException sqlex) { System.err.println(sqlex.getMessage()); }

    return row;
  }
// ---------------------------------------------------------------------------------------------- \\
  private static List<DTO> viewRowOperational(DATA_DOMAIN[] attrs, int PK) {

    final List<DTO> row = new ArrayList<>();

    try (final Connection conn = connect(Database.OPERATIONAL_DB);
         final PreparedStatement ps = conn.prepareStatement(SELECT_ROW_JOPERATIONAL)) {
         conn.setAutoCommit(false);

         ps.setInt(1, PK);
         ResultSet rs = ps.executeQuery();

         while (rs.next()) { // HERE IS THE PROBLEM A CONCRETE DATA STRUCTURE IS REQUIRED
           row.add(new JOperationalDTO(rs.getInt(getEntity(attrs[0])),
                                       rs.getString(getEntity(attrs[1])))); }

         conn.commit(); }
    catch (SQLException sqlex) { System.err.println(sqlex.getMessage()); }

    return row;
  }
// ---------------------------------------------------------------------------------------------- \\
  private static List<DTO> viewRowMaster(DATA_DOMAIN[] attrs, int PK) {

    final List<DTO> row = new ArrayList<>();

    try (final Connection conn = connect(Database.MASTER_DB);
         final PreparedStatement ps = conn.prepareStatement(SELECT_ROW_JMASTER)) {
         conn.setAutoCommit(false);

         ps.setInt(1, PK);
         ResultSet rs = ps.executeQuery();

         while (rs.next()) {
           row.add(new JMasterDTO(rs.getInt(getEntity(attrs[0])),
                                  rs.getString(getEntity(attrs[1])),
                                  rs.getString(getEntity(attrs[2])),
                                  rs.getString(getEntity(attrs[3])))); }

         conn.commit(); }
    catch (SQLException sqlex) { System.err.println(sqlex.getMessage()); }

    return row;
  }
// ---------------------------------------------------------------------------------------------- \\
  private static List<DTO> viewTableUnstructured(DATA_DOMAIN[] attr) {

    final List<DTO> table = new ArrayList<>();

    try (final Connection conn = connect(Database.UNSTRUCTURED_DB);
         PreparedStatement ps = conn.prepareStatement(SELECT_TABLE_JUNSTRUCTURED)) {
         conn.setAutoCommit(false);

         ResultSet rs = ps.executeQuery();

         while (rs.next()) {
           table.add(new JUnstructuredDTO(rs.getInt(getEntity(attr[0])),
                                          rs.getString(getEntity(attr[1])))); }

         conn.commit(); }
    catch (SQLException sqlex) { System.err.println(sqlex.getMessage()); }

    return table;
  }

  private static List<DTO> viewTableWarehouse(DATA_DOMAIN[] attr) {

    final List<DTO> table = new ArrayList<>();

    try (final Connection conn = connect(Database.ANALYTICAL_DB);
         PreparedStatement ps = conn.prepareStatement(SELECT_TABLE_JANALYTICAL)) {
         conn.setAutoCommit(false);

         ResultSet rs = ps.executeQuery();

         while (rs.next()) {
           table.add(new JAnalyticalDTO(rs.getInt(getEntity(attr[0])),
                                        rs.getString(getEntity(attr[1])))); }

         conn.commit(); }
    catch (SQLException sqlex) { System.err.println(sqlex.getMessage()); }

    return table;
  }

  private static List<DTO> viewTableOperational(DATA_DOMAIN[] attr) {

    final List<DTO> table = new ArrayList<>();

    try (final Connection conn = connect(Database.OPERATIONAL_DB);
         PreparedStatement ps = conn.prepareStatement(SELECT_TABLE_JOPERATIONAL)) {
         conn.setAutoCommit(false);

         ResultSet rs = ps.executeQuery();

         while (rs.next()) {
           table.add(new JOperationalDTO(rs.getInt(getEntity(attr[0])),
                                         rs.getString(getEntity(attr[1])))); }

         conn.commit(); }
    catch (SQLException sqlex) { System.err.println(sqlex.getMessage()); }

    return table;
  }

  private static List<DTO> viewTableMaster(DATA_DOMAIN[] attr) {

    final List<DTO> table = new ArrayList<>();

    try (final Connection conn = connect(Database.MASTER_DB);
         PreparedStatement ps = conn.prepareStatement(SELECT_TABLE_JMASTER)) {
         conn.setAutoCommit(false);

         ResultSet rs = ps.executeQuery();

         while (rs.next()) {
           table.add(new JMasterDTO(rs.getInt(getEntity(attr[0])),
                                    rs.getString(getEntity(attr[1])),
                                    rs.getString(getEntity(attr[2])),
                                    rs.getString(getEntity(attr[3])))); }

         conn.commit(); }
    catch (SQLException sqlex) { System.err.println(sqlex.getMessage()); }

    return table;
  }
// ---------------------------------------------------------------------------------------------- \\
  private static boolean update() throws SQLException { return false; }

  private static boolean delete() throws SQLException { return false; }

  private static void print() { System.out.println(); }
  private static void print(Object any) {
    String pretty = any.toString().startsWith("[") ? any.toString().substring(1, any.toString().length() - 1) : any.toString();
    System.out.println(pretty);
  }

  private static void usage() { print("Usage: java -classpath .:sqlite-jdbc-3.36.0.3.jar SQLiteBrooklyn run"); exit(1); }
// ---------------------------------------------------------------------------------------------- \\

  public static void main(String[] args) throws ExecutionException, InterruptedException {
    if (0 == args.length) usage();

    List<String> tables = List.of(DROP_TABLE_JUNSTRUCTURED,
                                  DROP_TABLE_JANALYTICAL,
                                  DROP_TABLE_JOPERATIONAL,
                                  DROP_TABLE_JMASTER);

    List<String> entities = List.of(CREATE_TABLE_JUNSTRUCTURED,
                                    CREATE_TABLE_JANALYTICAL,
                                    CREATE_TABLE_JOPERATIONAL,
                                    CREATE_TABLE_JMASTER);

    List<String> selectRows = List.of(SELECT_ROW_JUNSTRUCTURED,
                                      SELECT_ROW_JANALYTICAL,
                                      SELECT_ROW_JOPERATIONAL,
                                      SELECT_ROW_JMASTER);

    List<String> selectTables = List.of(SELECT_TABLE_JUNSTRUCTURED,
                                        SELECT_TABLE_JANALYTICAL,
                                        SELECT_TABLE_JOPERATIONAL,
                                        SELECT_TABLE_JMASTER);

    print("""

      ------------------------------------------------------------------------
      Stage 1: drop table if exists
      Stage 2: create tables junstructured, janalytical, joperational, jmaster
      Stage 3: insert into table attributes values
      Stage 4: select attributes from table where pk = 1
      Stage 5: select * from table
      ------------------------------------------------------------------------
      """);

    /* Database is of type enum witch matches on the primitive type int as ordinal 0,1,2,3.
     * A Future<?> demonstrates non-blocking Input/Output in a asynchronous hardware environment.
     */

    final int PK = 1;
    ExecutorService exec = Executors.newFixedThreadPool(4);

    for (Database db : Database.values()) {
      switch (db) {
        case UNSTRUCTURED_DB -> {
          drop(db, tables.get(0));
          create(db, entities.get(0));
          insertJUnstructured();
          print();

          Future<List<DTO>> row = exec.submit(() -> getRowDTO(db, JUnstructured.values(), PK));

          print(getTableDTO(db, JUnstructured.values()));
          print();
          print(row.get());
        }
        case ANALYTICAL_DB    -> {
          drop(db, tables.get(1));
          create(db, entities.get(1));
          insertJAnalytical();
          print();

          Future<List<DTO>> row = exec.submit(() -> getRowDTO(db, JAnalytical.values(), PK));

          print(getTableDTO(db, JAnalytical.values()));
          print();
          print(row.get());
        }
        case OPERATIONAL_DB  -> {
          drop(db, tables.get(2));
          create(db, entities.get(2));
          insertJOperational();
          print();

          Future<List<DTO>> row = exec.submit(() -> getRowDTO(db, JOperational.values(), PK));

          print(getTableDTO(db, JOperational.values()));
          print();
          print(row.get());
        }
        case MASTER_DB       -> {
          drop(db, tables.get(3));
          create(db, entities.get(3));
          insertJMaster();
          print();

          Future<List<DTO>> row = exec.submit(() -> getRowDTO(db, JMaster.values(), PK));

          print(getTableDTO(db, JMaster.values()));
          print();
          print(row.get());
        }
      }
    }

    print();
    exec.shutdown();
    print("""
      ------------------------------------------------------------------------
      All done.
      $ sqlite3 jmaster.db
      """);
  }
}

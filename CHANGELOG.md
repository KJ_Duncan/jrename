# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.0.1 - 2020-11-24

- patch: constructor PathMatcher instantiation moved to main method
- patch: constants PWD, SPACE, UNDERSCORE for '', ' ', '_'
- patch: variable depth improved cli parsing and Int.MAX_VALUE traversal
- patch: catch block adds NumberFormatException, SecurityException for main method



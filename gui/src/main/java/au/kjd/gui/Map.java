package au.kjd.gui;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

final class Map<K, V> {

  private final ConcurrentMap<K, V> map = new ConcurrentHashMap<>();

  static <K, V> Map<K, V> empty() { return new Map<>(); }

  static <K, V> Map<K, V> add(Map<K, V> m, K k, V v) { m.map.put(k, v); return m; }

  Option<V> get(final K k) { return this.map.containsKey(k) ? Option.some(this.map.get(k)) : Option.none(); }

  Map<K, V> put(K k, V v) { return add(this, k, v); }

  Map<K, V> removeKey(K k) { this.map.remove(k); return this; }
}


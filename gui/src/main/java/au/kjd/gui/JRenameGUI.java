package au.kjd.gui;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;


public final class JRenameGUI extends Application {

  // parse cli commands before JavaFX Application Thread starts
  @Override public void init() throws Exception {
    // process cli args: getParameters().getRaw();
    super.init();
  }

  @Override public void start(Stage stage) {
    stage.setTitle("jrename - multiple selection");
    stage.setScene(new Scene(new JRenameController().create()));
    stage.show();
  }

  // cleanup post Platform.exit call (database commits)
  @Override public void stop() throws Exception {
    super.stop();
  }

  public static void main(String[] args) { launch(args); }
}

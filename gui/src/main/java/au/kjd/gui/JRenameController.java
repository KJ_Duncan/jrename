package au.kjd.gui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;

/*
import javafx.beans.binding.Bindings;
import javafx.beans.property.Property;
import javafx.beans.property.ObjectProperty;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.control.CheckBox;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.scene.control.ChoiceBox;
 */

public final class JRenameController {

  /*
   * select * from JMaster loads database data into a Node
   * on each row maintain user selection state.
   * quit: terminates the application
   * run: starts the application
   *
   * on run: collapse gui into a button bar with three buttons; quit, halt, stats
   * on stats: show small gui with summary statistics and a box-and-whiskers plot
   * on halt: stop the application and write current state to database change button name to resume
   * on quit: write current application state to databases and terminate the application
   *
   * javafx 17 api: <https://openjfx.io/javadoc/17/index.html>
   *
   * 15.3 Registering Handlers and Handling Events, chpt 15, p.589
   *
   * ConcurrentHashMap<> computeIfPresent(key, function) OR
   * SQLite CREATE TEMPORARY VIEW are read-only
   *
   * If the "TEMP" or "TEMPORARY" keyword occurs in between "CREATE"
   * and "VIEW" then the view that is created is only visible to the
   * database connection that created it and is automatically deleted
   * when the database connection is closed. If a column-name list
   * follows the view-name, then that list determines the names of the
   * columns for the view.
   */

  Parent create() {

    GridPane pane = new GridPane();

    TableView<Model> tableView = new TableView<>();
    TableColumn<Model, Integer> columnI = new TableColumn<>("Id");
    TableColumn<Model, String> columnP  = new TableColumn<>("Pattern");
    TableColumn<Model, String> columnD  = new TableColumn<>("Dedupe");
    TableColumn<Model, String> columnF  = new TableColumn<>("FileType");

    tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

    // target.bind(source)
    tableView.getColumns().add(columnI);
    tableView.getColumns().add(columnP);
    tableView.getColumns().add(columnD);
    tableView.getColumns().add(columnF);

    // List<Dto> from dql.select();
    ObservableList<Model> models = FXCollections.observableArrayList(
        new JRenameModel(new Id(1), new Pattern("-.0-9A-Z_a-z"), new Dedupe("_"), new Filetype("*.*")),
        new JRenameModel(new Id(2), new Pattern("A-Za-z"),       new Dedupe("-"), new Filetype("jpg,png,jpeg")),
        new JRenameModel(new Id(3), new Pattern("0-5a-f"),       new Dedupe("%"), new Filetype("kak,kakrc")),
        new JRenameModel(new Id(4), new Pattern("6-9G-K"),       new Dedupe(","), new Filetype("pdf,docx,pptx"))
    );

    // load data from database
    models.forEach(entity -> tableView.getItems().add(entity));

    // capture user multiple selections
    tableView.getSelectionModel().selectedItemProperty().addListener(ov -> {
      System.out.println("Selected indices: " + tableView.getSelectionModel().getSelectedIndices());
      System.out.println("Selected items: " + tableView.getSelectionModel().getSelectedItem());
    });

    columnI.setCellValueFactory(new PropertyValueFactory<>("id"));
    columnP.setCellValueFactory(new PropertyValueFactory<>("pattern"));
    columnD.setCellValueFactory(new PropertyValueFactory<>("dedupe"));
    columnF.setCellValueFactory(new PropertyValueFactory<>("filetype"));

    Button quit = new Button("Quit");
    Button run  = new Button("Run");

    // stylesheets: <https://openjfx.io/javadoc/17/javafx.graphics/javafx/scene/doc-files/cssref.html>
    pane.setStyle("-fx-alignment: center; -fx-padding: 10.0; -fx-vgap: 10.0; -fx-hgap: 10.0;");
    quit.setStyle("-fx-border-color: orangered;");
    run.setStyle("-fx-border-color: limegreen;");

    quit.setOnAction(event -> javafx.application.Platform.exit());
    run.setOnAction(event -> {
      if (run.getText().startsWith("R")) { run.setText("Halt"); run.setStyle("-fx-border-color: sandybrown;"); }
      else { run.setText("Run"); run.setStyle("-fx-border-color: limegreen;"); }
    });

    pane.add(tableView, 0, 0);
    pane.add(quit, 0, 1);
    pane.add(run, 0, 1);

    GridPane.setHalignment(quit, HPos.LEFT);
    GridPane.setHalignment(run, HPos.RIGHT);

    return pane;
  }
}

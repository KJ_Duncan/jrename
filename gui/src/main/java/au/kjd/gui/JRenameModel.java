package au.kjd.gui;

sealed interface Model permits JRenameModel {}

final record Id(Integer field) {}
final record Pattern(String field) {}
final record Dedupe(String field) {}
final record Filetype(String field) {}

/* must be public and cannot be a record instance in javafx */
public final class JRenameModel implements Model {
  private final Id id;
  private final Pattern pattern;
  private final Dedupe dedupe;
  private final Filetype filetype;

  JRenameModel(final Id id,
               final Pattern pattern,
               final Dedupe dedupe,
               final Filetype filetype) {

    this.id       = id;
    this.pattern  = pattern;
    this.dedupe   = dedupe;
    this.filetype = filetype;
  }

  public Integer getId() { return id.field(); }

  public String getPattern() { return pattern.field(); }

  public String getDedupe() { return dedupe.field(); }

  public String getFiletype() { return filetype.field(); }
}

package au.kjd.gui;

abstract class Option<A> {

  @SuppressWarnings("rawtypes")
  private static final Option none = new None();
  abstract A getOrThrow();

  private static class None<A> extends Option<A> {

    private None() {};

    @Override A getOrThrow() { throw new IllegalStateException("get called on None"); }

    @Override public String toString() { return "None"; }
  }

  private static class Some<A> extends Option<A> {

    private final A value;

    private Some(A a) { value = a; }

    @Override A getOrThrow() { return this.value; }

    @Override public String toString() { return String.format("Some(%s)", this.value); }

  }

  static <A> Option<A> some(A a) { return new Some<>(a); }

  @SuppressWarnings("unchecked")
  static <A> Option<A> none() { return none; }
}


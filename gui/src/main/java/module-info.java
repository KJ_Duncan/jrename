module au.kjd.gui {
  requires javafx.base;
  requires javafx.controls;
  requires javafx.graphics;

  opens au.kjd.gui to javafx.base;
  exports au.kjd.gui to javafx.graphics;
}

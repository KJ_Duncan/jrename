plugins {
  id("jrename.java-application-conventions")
  /*
     using local jdk - java: 17.0.0.fx-librca

     only requires the below plugin if a jdk
     installation does not bundle javafx modules
     see <https://sdkman.io/jdks#librca>
   */
  // id("org.openjfx.javafxplugin") version "0.0.10"
}

// javafx {
  // version = "17"
  // modules("javafx.base", "javafx.controls", "javafx.graphics")
// }

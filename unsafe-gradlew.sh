#!/bin/sh

# ******************************************************************
# ALTERATIONS HAVE BEEN MADE TO THIS FILE WITHOUT GRADLES' CONSENT #
# ******************************************************************
#  PERSONAL PLAYGROUND: DO NOT USE. PLEASE POINT OUT ANY MISTAKES.
#
#  AMBITION: portable script with a smaller platform/os focus run
#            in a secure sub-shell not as a replacement for Gradle:
#            ./gradlew
#
# //////////////////////////////////////////////////////////////////
# ******************************************************************
# ******************************************************************
#
# Copyright 2015 the original author or authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# -------------------------------------------------------------------------- #
# Unset all possible aliases.
\unalias -a
# -------------------------------------------------------------------------- #
# Ensure getconf, command, ..., are not a user functions.
unset -f command getconf env printf type cd sed java ls dirname expr
# -------------------------------------------------------------------------- #
# Put on a reliable PATH prefix, as order matters.
# before: /usr/bin/*
# after: /bin/*
PATH="$(command -p getconf PATH):$PATH"
##############################################################################
##
##  Gradle start up script for UN*X
##
##############################################################################

JAVA_HOME_INVALID_DIRECTORY="ERROR: JAVA_HOME is set to an invalid directory: $JAVA_HOME

Please set the JAVA_HOME variable in your environment to match the
location of your Java installation."

JAVA_HOME_INVALID_PATH="ERROR: JAVA_HOME is not set and no 'java' binary could be found on your PATH.

Please set the JAVA_HOME variable in your environment to match the
location of your Java installation."

##############################################################################
## STDOUT FUNCTIONS
##############################################################################
#
gw_help() {
  cat 0<<_EOF_

Usage: ${0##*/} [help|:help|--help]

_EOF_
}

gw_warn() { env printf "%s \n" "$*" 1>&2; }

gw_die() { env printf "\n%s \n" "$*" 1>&2; exit 1; }

##############################################################################
## SHORT CIRCUIT
##############################################################################

if [ $# -eq 0 ]; then gw_help; exit; fi

if ! ( type java 1>/dev/null 2>&1 ); then gw_die "$JAVA_HOME_INVALID_PATH"; fi

##############################################################################
## STAGE 1 OF 7
##############################################################################
#
# OS specific support (must be 'true' or 'false').
gw_os() {

  darwin=false
  linux=false

  case $(uname) in
    Darwin* )
      darwin=true
      ;;
    Linux* )
      linux=true
      ;;
    * )
      gw_die "OS not supported."
      ;;
  esac

}

##############################################################################
## STAGE 2 OF 7
##############################################################################
#
JAVACMD=""

gw_jcmd() {

  # Determine the Java command to use to start the JVM.
  if [ -n "$JAVA_HOME" ];
    then if [ -x "$JAVA_HOME/bin/java" ];
           then JAVACMD="$JAVA_HOME/bin/java";
         fi
  fi
  
  if [ ! -x "$JAVACMD" ]; then gw_die "$JAVA_HOME_INVALID_DIRECTORY"; fi

}

##############################################################################
## STAGE 3 OF 7
##############################################################################
#
# Resolve links: $0 may be a link
PRG="$0"
gw_prg() {

  # Need this for relative symlinks.
  while [ -h "$PRG" ] ; do
    ls=$(ls --format=long --directory "$PRG")
    link=$(expr "$ls" : '.*-> \(.*\)$')
    if expr "$link" : '/.*' 1>/dev/null;
      then PRG="$link"
    else PRG=$(dirname "$PRG")/"$link"
    fi
  done

}

##############################################################################
## STAGE 4 OF 7
##############################################################################
#
# Attempt to set APP_HOME
gw_app_set() {

  SAVED=$(env pwd --physical)
  # symbolic links are resolved to their true values
  env cd -P $(dirname "$PRG") 1>/dev/null

  APP_HOME=$(env pwd --physical)
  env cd -P "$SAVED" 1>/dev/null

  APP_NAME="Gradle"
  APP_BASE_NAME=$(basename "$0")

  # Add default JVM options here. You can also use JAVA_OPTS
  # and GRADLE_OPTS to pass JVM options to this script.
  DEFAULT_JVM_OPTS='"-Xmx64m" "-Xms64m"'

  # Use the maximum available, or set MAX_FD != -1 to use that value.
  MAX_FD="maximum"

  CLASSPATH=$APP_HOME/gradle/wrapper/gradle-wrapper.jar
  
}

##############################################################################
## STAGE 5 OF 7
##############################################################################
#
# `ulimit` increase the maximum file descriptors if we can -H, find out why
# Muntaha, S 2019, Linux ulimit Command, LinuxHint, viewed 13 August 2021, <https://linuxhint.com/linux_ulimit_command/>
# reapedjuggler 2020, Ulimit, Soft Limits and Hard Limits in Linux, Geeks for Geeks, viewed 13 August 2021, <https://www.geeksforgeeks.org/ulimit-soft-limits-and-hard-limits-in-linux/>
# TEST: do we need to max out the users hard limit file descriptors to run gradle (check might be a requirement at an Enterprise level)
gw_ulimit() {

  if [ "$linux" = "true" ] ; then
    MAX_FD_LIMIT=$(ulimit -H -n)
    if [ $? -eq 0 ]; then
      if [ "$MAX_FD" = "maximum" ] || [ "$MAX_FD" = "max" ] ; then MAX_FD="$MAX_FD_LIMIT"; fi
      ulimit -n "$MAX_FD"
      if [ $? -ne 0 ] ; then gw_warn "Could not set maximum file descriptor limit: $MAX_FD"; fi
    else gw_warn "Could not query maximum file descriptor limit: $MAX_FD_LIMIT"
    fi
  fi

}

##############################################################################
## STAGE 6 OF 7
##############################################################################
## For Darwin, add options to specify how the application appears in the dock
gw_darwin() {

  if $darwin; then
    GRADLE_OPTS="$GRADLE_OPTS \"-Xdock:name=$APP_NAME\" \"-Xdock:icon=$APP_HOME/media/gradle.icns\""
  fi

}

##############################################################################
## STAGE 7 OF 7
##############################################################################
#
# call stack
gw_run() {
 
  gw_os
  gw_jcmd
  gw_prg
  gw_app_set
  gw_ulimit
  gw_darwin
  
}

##############################################################################
# RUN AND ... WON?
##############################################################################
gw_run

# Collect all arguments for the java command;
#   * $DEFAULT_JVM_OPTS, $JAVA_OPTS, and $GRADLE_OPTS can contain fragments of
#     shell script including quotes and variable substitutions, so put them in
#     double quotes to make sure that they get re-expanded; and
#   * put everything else in single quotes, so that it's not re-expanded.

set -- \
        "-Dorg.gradle.appname=$APP_BASE_NAME" \
        -classpath "$CLASSPATH" \
        org.gradle.wrapper.GradleWrapperMain \
        "$@"

# Use "xargs" to parse quoted args.
#
# With -n1 it outputs one arg per line, with the quotes and backslashes removed.
#
# In Bash we could simply go:
#
#   readarray ARGS < <( xargs -n1 <<<"$var" ) &&
#   set -- "${ARGS[@]}" "$@"
#
# but POSIX shell has neither arrays nor command substitution, so instead we
# post-process each arg (as a line of input to sed) to backslash-escape any
# character that might be a shell metacharacter, then use eval to reverse
# that process (while maintaining the separation between arguments), and wrap
# the whole thing up as a single "set" statement.
#
# This will of course break if any of these variables contains a newline or
# an unmatched quote.
#

eval "set -- $(
        printf '%s\n' "$DEFAULT_JVM_OPTS $JAVA_OPTS $GRADLE_OPTS" |
        xargs -n1 |
        sed ' s~[^-[:alnum:]+,./:=@_]~\\&~g; ' |
        tr '\n' ' '
    )" '"$@"'

exec "$JAVACMD" "$@"


package au.kjd.jrename;

import java.util.Objects;
import java.io.IOException;
import java.lang.SecurityException;
import java.lang.UnsupportedOperationException;

/* encapsulates thrown exceptions to be thrown at the call site:
 *   try { posix().compose(mv()).apply(path) } catch (exceptions) { at call site }
 *               --^^^^^^--------^^^^^---------^^^^^-- 
 * 
 * source adapted: Oracle 2021, Function<T, R>, java.util.function, viewed 12 May 2021,
 *                              <https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/util/function/Function.html>
 */
@FunctionalInterface public interface ExceptionalFunction<T, R> {
  R apply(T t) throws UnsupportedOperationException, IOException, SecurityException;

  /* ? super V is contravariance as consumer(in);  ? extends T is covariant as producer(): out  */
  default <V> ExceptionalFunction<V, R> compose(ExceptionalFunction<? super V, ? extends T> before) {
    Objects.requireNonNull(before);
    return (V v) -> apply(before.apply(v));
  }

  default <V> ExceptionalFunction<T, V> andThen(ExceptionalFunction<? super R, ? extends V> after) {
    Objects.requireNonNull(after);
    return (T t) -> after.apply(apply(t));
  }

  static <T> ExceptionalFunction<T, T> identity() { return t -> t; }
}

package au.kjd.jrename;

import java.util.function.LongSupplier;

/* DOCS: ExceptionalResult documentation
 *
 * Long is a subtype of Number, but Success<Long> is not a subtype of Success<Number>. To circumvent
 * this problem, use wildcard generic types. A wildcard generic type has three forms: <?> and
 * <? extends T>, as well as <? super T>, where T is a generic type (Liang 2015, p.748).
 *
 * 1) <?>, called an unbounded wildcard, is the same as ? extends Object.
 * 2) <? extends T>, called a bounded wildcard, represents T or a subtype of T.
 * 3) <? super T>, called a lower-bound wildcard, denotes T or a supertype of T.
 *
 * Liang (2015) explains that <? extends Number> is a wildcard type that represents Number or a
 * subtype of Number, so it is legal to invoke: new Success<Long>().
 *
 * default Result<?, ?> resultFrom(LongSupplier block) {
 *  try { return new Success<>(block.getAsLong()); }
 *  catch (Throwable ex) { return new Failure<>(ex); }
 * }
 *
 * static Result<?, ?> resultFrom(LongSupplier block) {
 *  try { return new Success<Long>(block.getAsLong()); }
 *  catch (Throwable ex) { return new Failure<Throwable>(ex); }
 * }
 *
 * <? super T> is contravariance as consumer(in)
 * <? extends T> is covariant as producer(): out
 *
 * $ javap -private -s Result<out S, out F>.kt -- abstract class extends
 *
 *
 * Kotlin 2021, 'Generics: in, out, where', kotlinlang dot org, viewed 15 May 2021,
 *             <https://kotlinlang.org/docs/generics.html>
 *
 * Langer, K 2015, Frequently Asked Questions, Java Generics, viewed 15 May 2021,
 *                <http://www.angelikalanger.com/GenericsFAQ/JavaGenericsFAQ.html>
 *
 * Liang, Y. D 2015, 19.7 Wildcard Generic Types, Generics, Introduction to Java Programming,
 *          Comprehensive Version, Tenth Edition, Chpt 19, pp. 738-59, Armstrong Atlantic State University, Pearson Publishing.
 *
 * Oracle 2021, The javap Command, Java Development Kit Version 16 Tool Specifications, viewed 15 May 2021,
 *              <https://docs.oracle.com/en/java/javase/16/docs/specs/man/javap.html>
 *
 * Vantasin, K et al. 2021, Result.kt, v4.0.0, viewed 08 May 2021,
 *         <https://github.com/kittinunf/Result/blob/master/result/src/main/kotlin/com/github/kittinunf/result/Result.kt#L122>
 *
 */
public sealed interface ExceptionalResult<S, F> permits EFailure, ESuccess {
  /* 
   * Liang (2015) a static method cannot invoke an instance method or access an instance data field,
   * since static methods and static data fields don’t belong to a particular object.
   *
   * Liang, Y. D 2015, 9.7  Static Variables, Constants, and Methods, Objects and Classes, Introduction to Java Programming,
   *           Comprehensive Version, Tenth Edition, Chpt 9, p.340, Armstrong Atlantic State University, Pearson Publishing.
   *
   * Oracle 2021, Interface LongSupplier, java.util.function, java se 16, viewed 28 May 2021,
   *              <https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/util/function/LongSupplier.html>
   *
   * resutl4k 2019, result.kt, package com.natpryce, viewed 12 May 2021,
   *                <https://github.com/npryce/result4k/blob/master/src/main/kotlin/com/natpryce/result.kt#L14>
   */
  static ExceptionalResult<?, ?> resultFrom(LongSupplier block) { /* Long throws NumberFormatException, SecurityException */
    try { return new ESuccess<>(block.getAsLong()); }
    catch (Throwable ex) { return new EFailure<>(ex); }
  }
}

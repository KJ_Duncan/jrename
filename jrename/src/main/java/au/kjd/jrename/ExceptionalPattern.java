package au.kjd.jrename;

import java.sql.SQLException;
import java.sql.BatchUpdateException;
import java.sql.SQLRecoverableException;
import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.nio.file.AtomicMoveNotSupportedException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.FileSystemException;
import java.nio.file.Path;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.util.StringBuilderFormattable;

import static java.lang.String.format;

sealed interface ExceptionalPattern permits ExceptionalMatch {
  /*
     placeholder idea for exceptional logging subject to CWE 2007 & OWASP 2021

     Oracle 2021, Class Throwable, Java SE 16, viewed 03 June 2021,
                  <https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/lang/Throwable.html>

     Romance, M-C 2006, Teenagers, The Black Parade, viewed 03 June 2021,
                        <https://open.spotify.com/track/7j31rVgGX9Q2blT92VBEA0?si=707bcd602a224654>
   */
}

/* a thought: we could 'implements Message' and encapsulate our ExceptionalMatch logic
      <https://logging.apache.org/log4j/2.x/manual/messages.html>

  Syslog Message Format, <https://datatracker.ietf.org/doc/html/rfc5424#section-6> */
final record ExceptionalMatch(Path path, Throwable throwable) implements ExceptionalPattern, StringBuilderFormattable {
  private static final Logger logger = LogManager.getLogger();

  // REMOVE: in favour of owsap log(SecurityMarkers)
  private sealed interface Risk permits Peril, Hazard {}
  private final record Peril(String path) implements Risk, StringBuilderFormattable {
    @Override public void formatTo(StringBuilder buffer) { buffer.append(path()); }
  }
  private final record Hazard(String path) implements Risk, StringBuilderFormattable {
    @Override public void formatTo(StringBuilder buffer) { buffer.append(path()); }
  }

  // here may give us the opportunity to wrap and parse the exception in our own type: return Peril(msg)
  // whe can implement a Marker's here or at the call site?
  // generalised exceptions IOException and SecurityException can abstract away from call site .
  // TODO: owsap log.info(SecurityMarkers.SECURITY_FAILURE, "Access control check just failed") 
  private Risk exceptional() {
    if (null == throwable) return new Peril("null");

    // this will become a large nested conditional statement for finer
    // grain logging/markers/failure information requirements
    if (throwable instanceof IOException ioe) {
      if (ioe instanceof FileSystemException fse) {
        if (fse instanceof AccessDeniedException ade) { return hazard(); }
        else if (fse instanceof AtomicMoveNotSupportedException amnse) { return hazard(); }
        else if (fse instanceof FileAlreadyExistsException faee) { return hazard(); }
        else return peril();
      }
      else return peril();
    }

    else if (throwable instanceof SQLException se) {
      if (se instanceof BatchUpdateException be) {}
      else if (se instanceof SQLRecoverableException re) {}
      else return sql(se);
    }

    else return peril();
    return null;
  }

  // placeholder for implementation of logging/marker/parser here
  private Risk hazard() { return new Hazard(format("%s: %s", path().toAbsolutePath(), throwable().fillInStackTrace())); }

  private Risk peril() { return new Peril(format("%s: %s", path().toAbsolutePath(), throwable().fillInStackTrace())); }

  // 8.1.2 Navigating SQLExceptions, JDBC 4.3, chpt 8, pp.44-45
  // import java.sql.SQLException;
  // import java.sql.BatchUpdateException;
  // import java.sql.SQLRecoverableException;

  // chained exceptions
  private Risk sql(SQLException se) {
    while (null != se) {
      logger.warn(format("SQLState: %s%nError Code: %s%nMessage: %s",
                    se.getSQLState(), se.getErrorCode(), se.getMessage()));

      Throwable t = se.getCause();
      while (null != t) {
        logger.warn(format("Cause: %s", t));
        t = t.getCause();
      }
      se = se.getNextException();
    }
    return new Hazard("sql");
  }

  /* Garbage free logging: Objects that implement StringBuilderFormattable can be converted to text,
   *                       ideally without allocating temporary objects.
   *                       If an object is logged that implements this interface,
   *                       its formatTo method is called instead of toString().
   *                       <https://logging.apache.org/log4j/2.x/manual/garbagefree.html>
   */
  @Override public void formatTo(StringBuilder buffer) { buffer.append(exceptional()); }
}

/* catch the expected

 $ ./gradlew test --tests "au.kjd.jrename.ExceptionalMatchTest"
 > Task :test FAILED
 > PASSED: Dumont, D & Ross, S 2019, Red Light Green Light, For Club Play Only, Pt.6, viewed 02 June 2021,
                                    <https://open.spotify.com/track/5D1zGVDJTOqwaGSzYw5TtP?si=1b93446a34c04912>

The order in which the exception handlers of a method are searched for a match is
important. Within a class file, the exception handlers for each method are stored in
a table (s.4.7.3). At run time, when an exception is thrown, the Java Virtual Machine
searches the exception handlers of the current method in the order that they appear
in the corresponding exception handler table in the class file, starting from the
beginning of that table (s.2.10 Exceptions).

So, order matters as IOException generalises 'Direct Known Subclasses' which
we don't want to catch on a long running jvm.

TEST: An abrupt death ok on ATOMIC_MOVE and can we log last position in a file-tree hierarchy as a return point on next startup?

Error, RuntimeException and their subclasses are collectively known as unchecked
exceptions. [...] Java runtime environment searches backward through the call stack to find
any methods that are interested in handling a particular exception. [...] A method that throws
an uncaught, checked exception must include a throws clause in its declaration. [...] The try
statement should contain at least one catch block or a finally block and may have multiple catch
blocks (ref: java tutorial).

Oracle 2021, Advantages of Exceptions, The Java Tutorials, viewed 01 June 2021,
             <https://docs.oracle.com/javase/tutorial/essential/exceptions/advantages.html>

Oracle 2021, 2.10. Exceptions, The Java® Virtual Machine Specification, Java SE 16, viewed 31 May 2021,
             <https://docs.oracle.com/javase/specs/jvms/se16/html/jvms-2.html#jvms-2.10>

Oracle 2021, Class IOException, package java.io, Java SE 16, viewed 31 May 2021,
             <https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/io/IOException.html>


problem: java.nio.file.Files.move() throws a generalised IOException
how about a wrapper exception class or method that checks for a SetOf<specific exceptions> else throws Generalised<IOException>: org.junit.jupiter.api.Assertions.assertThrows()
RESEARCH: derby database throws exceptions as a result <http://svn.apache.org/repos/asf/db/derby/code/trunk/java/org.apache.derby.tests/org/apache/>

                   walk throws: IllegalArgumentException, SecurityException, IOException
setPosixFilePermissions throws: UnsupportedOperationException, ClassCastException, IOException, SecurityException, RuntimePermission("accessUserInformation") <- perms for rw- --- ---;
                   move throws: UnsupportedOperationException, FileAlreadyExistsException, DirectoryNotEmptyException, AtomicMoveNotSupportedException, IOException, SecurityException
                                where DirectoryNotEmptyException = REPLACE_EXISTING
                                      AtomicMoveNotSupportedException = ATOMIC_MOVE

IOException:
  AttachOperationFailedException,
  ChangedCharSetException,
  CharacterCodingException,
  CharConversionException,
  ClosedChannelException,
  ClosedConnectionException,
  EOFException,
  FileLockInterruptionException,
  FileNotFoundException,
  FilerException,
- FileSystemException:
o   AccessDeniedException,            <-- Checked exception thrown when a file system operation is denied, typically due to a file permission or other access check.
o   AtomicMoveNotSupportedException,  <-- Checked exception thrown when a file cannot be moved as an atomic file system operation.
    DirectoryNotEmptyException,
o   FileAlreadyExistsException,       <-- Checked exception thrown when an attempt is made to create a file or directory and a file of that name already exists.
x   FileSystemLoopException,          <-- LinkerOption.FOLLOW_LINKS
    NoSuchFileException,
    NotDirectoryException,
    NotLinkException,
  HttpRetryException,
  HttpTimeoutException,
  IIOException,
  InterruptedByTimeoutException,
  InterruptedIOException,
  InvalidPropertiesFormatException,
  JMXProviderException,
  JMXServerErrorException,
  MalformedURLException,
  ObjectStreamException,
  ProtocolException,
  RemoteException,
  SaslException,
  SocketException,
  SSLException,
  SyncFailedException,
  TransportTimeoutException,
  UnknownHostException,
  UnknownServiceException,
  UnsupportedEncodingException,
  UserPrincipalNotFoundException,
  UTFDataFormatException,
  WebSocketHandshakeException,
  ZipException

x SecurityException:                           <-- Thrown by the security manager to indicate a security violation.
  o AccessControlException,                    <-- The reason to deny access can vary. <https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/security/AccessControlException.html>
    RMISecurityException,
  x RuntimePermission("accessUserInformation") <-- needed for file restriction ug+rw,ug-x,o-rwx : rw- rw- ---
    o NullPointerException - if name is null.
    o IllegalArgumentException - if name is empty.
  
  TODO: not comfortable with an automation system raising file privileges above rw for owner and group. show the user where and how.md: $ find . -type f -exec chmod o+r '{}' \;

IllegalArgumentException:
  IllegalChannelGroupException,
  IllegalCharsetNameException,
  IllegalFormatException,
  IllegalSelectorException,
  IllegalThreadStateException,
  InvalidKeyException,
  InvalidOpenTypeException,
  InvalidParameterException,
  InvalidPathException,
  InvalidStreamException,
  KeyAlreadyExistsException,
- NumberFormatException,                <-- LongSupplier
  PatternSyntaxException,
  ProviderMismatchException,
  UnresolvedAddressException,
  UnsupportedAddressTypeException,
  UnsupportedCharsetException

RuntimeException:
  AnnotationTypeMismatchException,
  ArithmeticException,
  ArrayStoreException,
  BufferOverflowException,
  BufferUnderflowException,
  CannotRedoException,
  CannotUndoException,
  CatalogException,
x ClassCastException,           <-- contains elements not of tpye PosixFilePermission
  ClassNotPreparedException,
  CMMException,
  CompletionException,
  ConcurrentModificationException,
  DateTimeException,
  DOMException,
  DuplicateRequestException,
  EmptyStackException,
  EnumConstantNotPresentException,
  EventException,
  FileSystemAlreadyExistsException,
  FileSystemNotFoundException,
  FindException,
  IllegalArgumentException,
  IllegalCallerException,
  IllegalMonitorStateException,
  IllegalPathStateException,
  IllegalStateException,
  IllformedLocaleException,
  ImagingOpException,
  InaccessibleObjectException,
  IncompleteAnnotationException,
  InconsistentDebugInfoException,
x IndexOutOfBoundsException,         <-- main(String[] args) throws IndexOutOfBoundsException
  InternalException,
  InvalidCodeIndexException,
  InvalidLineNumberException,
  InvalidModuleDescriptorException,
  InvalidModuleException,
  InvalidRequestStateException,
  InvalidStackFrameException,
  JarSignerException,
  JMRuntimeException,
  JSException,
  LayerInstantiationException,
  LSException,
  MalformedParameterizedTypeException,
  MalformedParametersException,
  MirroredTypesException,
  MissingResourceException,
  NativeMethodException,
  NegativeArraySizeException,
  NoSuchDynamicMethodException,
  NoSuchElementException,
  NoSuchMechanismException,
  NullPointerException,
  ObjectCollectedException,
  ProfileDataException,
  ProviderException,
  ProviderNotFoundException,
  RangeException,
  RasterFormatException,
  RejectedExecutionException,
  ResolutionException,
  SecurityException,
  SPIResolutionException,
  TypeNotPresentException,
  UncheckedIOException,
  UndeclaredThrowableException,
  UnknownEntityException,
  UnmodifiableModuleException,
  UnmodifiableSetException,
x UnsupportedOperationException,  <-- file system does not support the PosixFileAttributeView
  VMDisconnectedException,
  VMMismatchException,
  VMOutOfMemoryException,
  WrongMethodTypeException,
  XPathException

*/

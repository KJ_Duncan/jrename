package au.kjd.jrename;

public final record EFailure<F extends Throwable>(F failure) implements ExceptionalResult<Void, F> { }

package au.kjd.jrename;

/* TODO: Numerical Descriptive measures implementation */
sealed interface Numerical permits BoxPlot, Descriptive { }

final record Descriptive(int in) implements Numerical { }

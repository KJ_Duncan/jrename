package au.kjd.jrename;

import java.io.IOException;
import java.lang.SecurityException;
import java.lang.UnsupportedOperationException;
import java.util.Objects;

@FunctionalInterface public interface ExceptionalBiFunction<T, U, R> {
  R apply(T t, U u) throws UnsupportedOperationException, IOException, SecurityException;

  // ExceptionalBiFunction<? super R, ? super V, ? extends V>
  default <V> ExceptionalBiFunction<T, U, V> andThen(ExceptionalFunction<? super R, ? extends V> after) {
    Objects.requireNonNull(after);
    return (T t, U u) -> after.apply(apply(t, u));
  }
}


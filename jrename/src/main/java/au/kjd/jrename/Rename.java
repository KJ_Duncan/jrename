package au.kjd.jrename;

import java.io.FilePermission;
import java.io.IOException;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.FileVisitor;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.security.Permission;
import java.util.Hashtable;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import static java.lang.Integer.MIN_VALUE;
import static java.lang.Integer.parseInt;
import static java.lang.Math.abs;
import static java.lang.Runtime.getRuntime;
import static java.lang.String.format;
import static java.lang.System.out;
import static java.nio.file.attribute.PosixFilePermission.GROUP_READ;
import static java.nio.file.attribute.PosixFilePermission.GROUP_WRITE;
import static java.nio.file.attribute.PosixFilePermission.OWNER_READ;
import static java.nio.file.attribute.PosixFilePermission.OWNER_WRITE;
import static java.nio.file.Files.move;
import static java.nio.file.Files.setPosixFilePermissions;
import static java.nio.file.FileSystems.getDefault;
import static java.nio.file.StandardCopyOption.ATOMIC_MOVE;
import static java.util.EnumSet.of;
import static java.util.regex.Pattern.compile;


/**
 * Rename traverses current working directory at a specified depth
 * collecting and renaming all files that match a user defined glob
 * or regex pattern. File names which consist of white space(s) are
 * renamed in place with underscores.
 *
 * File permission is set to rw-------.
 *
 *  Example 	      Description
 *  *.java 	        Matches a path that represents a file name ending in .java
 *  *.* 	          Matches file names containing a dot
 *  *.{java,class} 	Matches file names ending with .java or .class
 *  foo.? 	        Matches file names starting with foo. and a single character extension
 *  /home/*\/*      Matches/home/gus/data on UNIX platforms
 *  /home/**        Matches /home/gus and /home/gus/data on UNIX platforms
 *
 * USAGE:
 *  $ cd $HOME/Downloads/
 *  $ jrename 5 -glob "*.{docx,pptx,pdf}"
 *
 * @author Oracle 2020, 'FileSystem.getPathMatcher', java.nio.file, Java SE 15, viewed 24 November 2020,
 *         https://docs.oracle.com/en/java/javase/15/docs/api/java.base/java/nio/file/FileSystem.html
 */
final class Rename { /* kakoune TODO|FIXME|etc: grep-buffer.kak <https://bitbucket.org/snippets/KJ_Duncan/KrGkBa/grep-bufferkak.git> */

  private static final Logger logger = LogManager.getLogger(Rename.class);

  private static final String PWD        = "";
  private static final String NIL        = "";
  private static final String GLOB       = "glob";
  private static final String REGEX      = "regex";
  private static final String READ_WRITE = "read,write";
  private static final String UNDERSCORE = "_";


  /* TODO: regex patterns will be pulled from database::table jmaster
   * problem: ensure escape sequence in users regular expression PATTERN
   */
  private static final Pattern PATTERN        = compile("[^\\-.0-9A-Z_a-z]");
  private static final Pattern DEDUPLICATE    = compile(format("\\B(%s)+\\B", UNDERSCORE));
  // format() where %1$ = argument index and s = s.toString() in (%1$s)
  private static final Pattern PATTERN_GROUPS = compile(format("^(%1$s)+\\B|\\b(?<=\\.)(%1$s)+\\B|\\B(%1$s)+(?=\\.)\\b|(%1$s)+$", UNDERSCORE));

  private static final Set<PosixFilePermission> GROUP_RW = of(OWNER_READ, OWNER_WRITE,
                                                              GROUP_READ, GROUP_WRITE);

  /* private constructor */
  private Rename() { }

  /* in a final class declaration this is implicitly set */
  @Override protected Object clone() throws CloneNotSupportedException { throw new CloneNotSupportedException(); }

  /* jrename --gui
   *
   * javafx application on launch reads from database JMaster and populates the
   * validation form. User selects/ticks adjacent box to regular expression and
   * matching file type. System validates patterns and user selects ok to continue.
   * |-----------------|
   * |-|     | |     |-|
   * |-|-----|-|-----|-|
   * |-|_____| |_____|-|
   * |-----------------|
   *
   *  Capturing groups are numbered by counting their opening parentheses from left to right.
   *  In the expression ((A)(B(C))), for example, there are four such groups:
   *
   *  1. ((A)(B(C)))
   *  2. (A)
   *  3. (B(C))
   *  4. (C)
   *
   * Group zero always stands for the entire expression.
   *
   * ^(?<prefix>_)+\B|(?<pattern>[^-.0-9A-Z_a-z])|\B(?<dedupe>_)+\B|\B(?<prefix_ext>_)+(?=\.)\b|\b(?<=\.)(?<suffix_ext>_)+\B|(?<suffix>_)+$
   * ref: <https://regex101.com/r/vrPDK9/3>
   *
   *
   * Saumont, P-Y 2017, 2.3.4 Polymorphic higher-order functions, Functional Programming in Java, chpt 2, pp.36-38, Manning Publications.

  // (c -> d) -> (b -> c) -> (a -> b) -> (a -> d)
  interface Compose<A, B, C, D> extends Function<Function<C, D>, Function<Function<B, C>, Function<Function<A, B>, Function<A, D>>>> {}

  // f(g(h(x))) : returns functions to compose
  private static Compose<Path, String, String, String> compose() { return f -> g -> h -> x -> f.apply(g.apply(h.apply(x))); }

  // dedupe(groups(rename(x))
  //                       Path -> String
  // (c -> d) -> (b -> c) -> (a -> b) -> (a -> d)
  compose().apply(dedupe())
           .apply(groups())
           .apply(rename())
           .apply(dir);

   */

  private static Function<Path, String> rename() {

    return path -> PATTERN.matcher(path.getFileName().toString())
                          .replaceAll(UNDERSCORE);
  }

  /* required for removal of the underscore character '_.' */
  private static Function<String, String> groups() {

    return str -> PATTERN_GROUPS.matcher(str)
                                .replaceAll(NIL);
  }

  /* required for deduplication of the underscore character '__' */
  private static Function<String, String> dedupe() {

   return str -> DEDUPLICATE.matcher(str)
                            .replaceAll(NIL);
  }

  /* a helper function for mv() Pattern, Pattern, Pattern, Path, String */
  private static Function<Path, String> pipeline() {

    return path -> rename()
                   .andThen(groups())
                   .andThen(dedupe())
                   .apply(path);
  }

  private static ExceptionalFunction<Path, Path> mv() {

    return path -> move(path,
                        path.resolveSibling(pipeline().apply(path)),
                        ATOMIC_MOVE);
  }

  private static ExceptionalFunction<Path, Path> posix() {

    return path -> setPosixFilePermissions(path, GROUP_RW);
  }

  /* TODO: FileVisitor<Path> pencil and paper the events (preconditions, post-conditions) then plantuml sequence diagram <https://docs.oracle.com/javase/tutorial/essential/io/walk.html>

     static Path Files.walkFileTree(Path start,
                                    Set<FileVisitOption> options,
                                    int maxDepth,
                                    FileVisitor<? super Path> visitor) throws IOException

     FileVisitResult.CONTINUE
                     Continue without visiting the siblings of this file or directory.
                     SKIP_SIBLINGS
                     Continue without visiting the entries in this directory.
                     SKIP_SUBTREE
                     TERMINATE

  */
  static final class Walker implements FileVisitor<Path> {

    private static final Logger logger = LogManager.getLogger(Walker.class);

    // Invoked before a directory's entries are visited.
    @Override public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {

      return FileVisitResult.CONTINUE;
    }

    /*
       problem: if value present in application layer call data layer TEMPORARY VIEW for attribute
       problem: if key present in application layer call ConcurrentHashMap<> computeIfPresent for value
       problem: memorise filetypes in CREATE TEMPORARY VIEW or ConcurrentHashMap<>
       problem: both use memory allocation so which is best for: low resources, low consumption, consistent results?

       problem: backlog of filetypes to process, Walker walks not waits
       problem: know your data structures, now its getting interesting
       solution: pickup a book and read it.
      */
    @Override public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {

      // NOTE: FileVisitor<Path> is weakly consistent
      try {
        // problem: compute every file type that passes through here.
      } catch (Throwable t) { logger.warn(new ExceptionalMatch(file, t)); }

      return FileVisitResult.CONTINUE;
    }

    /* Invoked when the file cannot be accessed. The specific exception is passed to the method.
       TODO: visitFileFailed - see also JDBCAppender and FailoverAppender */
    @Override public FileVisitResult visitFileFailed(Path file, IOException exc) {
      if (null != exc) logger.warn(new ExceptionalMatch(file, exc));

      return FileVisitResult.CONTINUE;
    }

    /* here is our event isDirectory and passes any/all exceptions to us for logging
       Invoked after all the entries in a directory are visited. If any errors are
       encountered, the specific exception is passed to the method. */
    @Override public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
      if (null != exc) logger.warn(new ExceptionalMatch(dir, exc));
      // TODO: Numerical.java statistics

      return FileVisitResult.CONTINUE;
    }
  }

  /* unchecked exception thrown to indicate a syntax error in the regular-expression pattern */
  private static Function<String, Pattern> check() throws PatternSyntaxException { return Pattern::compile; }

  private static void usage() {
    out.print("""
      Usage:    jrename <depth> [-glob|-regex] "<pattern>"
      <depth>:  0 < depth <= 2^31-1 (Integer.MAX_VALUE)
      [-glob]:  https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/nio/file/FileSystem.html
      [-regex]: https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/util/regex/Pattern.html
    """);
    out.flush();
  }

  /* jrename entry point
   *
   * If a client can reasonably be expected to recover from an exception, make it a
   * checked exception. If a client cannot do anything to recover from the exception,
   * make it an unchecked exception (Java Tutorials).
   *
   * @author Oracle 2020, Unchecked Exceptions — The Controversy, The Java Tutorials, viewed 28 November 2020,
   *         https://docs.oracle.com/javase/tutorial/essential/exceptions/runtime.html
   *
   * FIXME: implement main method arguments: jrename --pattern='[^\\java_regex]' --file-types="*.*,or,a,comma,sep,list"
   *                                         jrename --gui
   */
  public static void main(final String[] args) {

    if (MIN_VALUE == args.length + 1 ||
        3 > args.length) { usage(); logger.fatal(args); getRuntime().exit(-1); }

    logger.info(args);

    try { final int depth = abs(parseInt(args[0]));

          // TODO: parse cli args into a Container.of(Pattern)'s
          PathMatcher pm = (args[1].matches("g|-g|glob|-glob")) ? getDefault()
                                                                .getPathMatcher(format("%s:%s",
                                                                                       GLOB,
                                                                                       args[2]))
                                                              : getDefault()
                                                                .getPathMatcher(format("%s:%s",
                                                                                       REGEX,
                                                                                       args[2]));
    }

    /*
       CWE 2007, Information Leakage and Improper Error Handling, OWASP Top Ten 2007 Category A6,
                 viewed 03 May 2021, <https://cwe.mitre.org/data/definitions/717.html>

       OWASP 2021, OWASP Secure Logging Benchmark, Open Web Application Security Project, viewed 16 May 2021,
                   <https://owasp.org/www-project-secure-logging-benchmark/>

       Morricone, E 1966, The Good, The Bad and The Ugly - Il Buono, Il Brutto, Il Cattivo, Spotify, viewed 29 April 2021,
                          <https://open.spotify.com/track/2IJJszwGK4NWmh3bNK6CPD?si=370ab02918384a92>
     */
    catch (Throwable t) { logger.catching(Level.ERROR, t); }

    finally { out.printf("%n%s%n", "rename.java exiting..."); out.flush(); }
  }
}

package au.kjd.jrename;

public final record ESuccess<S>(S success) implements ExceptionalResult<S, Void> { }

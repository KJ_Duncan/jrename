package au.kjd.jrename;

import java.io.IOException;
import java.sql.SQLException;
import java.util.function.Supplier;
import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;
import org.apache.logging.log4j.message.StructuredDataMessage;
import org.apache.logging.log4j.EventLogger;

import org.owasp.security.logging.util.SecurityUtil;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

/*
Observations:
21:55:58.906 [Test worker] INFO  au.kjd.jrename.Log4J2Test.LogLevelInfoTest - LOGGER Level.INFO: o
21:55:58.906 [Test worker] INFO  au.kjd.jrename.Log4J2Test - LOGGER Level.INFO: o

21:55:58.837 [Test worker] FATAL au.kjd.jrename.Log4J2Test.LogLevelFatalTest - LOGGER Level.FATAL: f
21:55:58.839 [Test worker] FATAL au.kjd.jrename.Log4J2Test - LogLevelFatalTest - LOGGER Level.FATAL: f

mLog function does not return the enclosing class name unless explicitly stated.
mLog an additional call to format is required to add additional details.
mLog does not require a static final declaration
mLog has an additional explicit conditional check
mLog is code duplication with no call site flexibility
 */
class Log4J2Test {

  private static final Logger LOGGER = LogManager.getLogger(Log4J2Test.class);

  /* Conditional Deferred Execution, Refactoring, testing, and debugging, Modern Java in Action, chpt 9, p.222 */
  private static void mLog(Level level, Supplier<String> msgSupplier)
  { if(LOGGER.isEnabled(level)) LOGGER.log(level, msgSupplier.get()); }

  private static void mLog(Level level, Throwable throwable, Supplier<String> msgSupplier)
  { if(LOGGER.isEnabled(level)) LOGGER.atLevel(level).withThrowable(throwable).log(msgSupplier.get()); }

  private static void mLog(Level level, SQLException throwable, Supplier<String> msgSupplier)
  { if(LOGGER.isEnabled(level)) LOGGER.atLevel(level).withThrowable(throwable).log(msgSupplier.get()); }

  @Nested
  class LogLevelInfoTest {
    private static final String MSG = "LOGGER Level.INFO";

    private static final Logger LOGGER = LogManager.getLogger(LogLevelInfoTest.class);

    @ParameterizedTest(name = "pattern of {0}")
    @ValueSource(strings = { "i", "n", "f", "o" })
    void test_log_level_info(String delimiters) {
      LOGGER.log(Level.INFO, "LOGGER Level.INFO: {}", () -> delimiters);
      mLog(Level.INFO, () -> String.format("%s: %s", MSG, delimiters));
      assertTrue(true);
    }
  }

  @Nested
  class LogLevelWarnTest {
    private static final String MSG = "LOGGER Level.WARN";

    private static final Logger LOGGER = LogManager.getLogger(LogLevelWarnTest.class);

    @ParameterizedTest(name = "pattern of {0}")
    @ValueSource(strings = { "w", "a", "r", "n" })
    void test_log_level_warn(String delimiters) {
      LOGGER.log(Level.WARN, "LOGGER Level.WARN {}", delimiters);
      assertTrue(true);
    }
  }

  @Nested
  class LogLevelErrorTest {
    private static final String MSG = "LOGGER Level.ERROR";

    private static final Logger LOGGER = LogManager.getLogger(LogLevelErrorTest.class);

    @ParameterizedTest(name = "pattern of {0}")
    @ValueSource(strings = { "e", "r", "r", "o", "r" })
    void test_log_level_error(String delimiters) throws SQLException {
      LOGGER.traceEntry("Parameters {}", () -> delimiters);

      LOGGER.log(Level.ERROR, "LOGGER Level.ERROR: {}", delimiters);

      // [Test worker] ERROR au.kjd.jrename.Log4J2Test.LogLevelErrorTest - Catching
      assertDoesNotThrow(() -> LOGGER.catching(Level.ERROR, new SQLException("getCause() = Catching, not SQLException").getNextException()));

      /* The throwing() method can be used by an application when it is throwing
         an exception that is unlikely to be handled, such as a RuntimeException.
         [Test worker] ERROR au.kjd.jrename.Log4J2Test.LogLevelErrorTest - Throwing
        
         Works by doing:
         throw LOGGER.throwing(Level.ERROR, new SQLException("getCause() = me!"));
      */
      assertDoesNotThrow(() -> LOGGER.throwing(Level.ERROR, new SQLException("getCause() = Throwing, not SQLException")));

      /* The catching() method can be used by an application when it catches an
         Exception that it is not going to rethrow, either explicitly or attached
         to another Exception.

         Logger.catching() why was it not throwing?
         log4j2-test.properties
           %xEx ignores exceptions while %throwables doesn't (stacktrace to test.log)
           see PatternLayout and Event Logging <https://logging.apache.org/log4j/2.x/manual/eventlogging.html>
       */
      assertDoesNotThrow(() -> LOGGER.catching(Level.ERROR, new IOException("LOGGER Level.ERROR")));

      LOGGER.traceExit();
    }
  }

  @Nested
  class LogLevelFatalTest {
    private static final String MSG = "LOGGER Level.FATAL";

    private static final Logger LOGGER = LogManager.getLogger(LogLevelFatalTest.class);

    @ParameterizedTest(name = "pattern of {0}")
    @ValueSource(strings = { "f", "a", "t", "a", "l" })
    void test_log_level_fatal(String delimiters) {
      LOGGER.atFatal().withLocation().log("LOGGER Level.FATAL: {}", () -> delimiters);
      // review the stacktrace error in resources/test.log
      assertDoesNotThrow(() -> mLog(Level.FATAL, new Throwable(MSG), () -> delimiters));
      assertDoesNotThrow(() -> LOGGER.atFatal().withLocation().withThrowable(new Throwable(MSG)).log("LOGGER Level.FATAL: {}", () -> delimiters));
      assertDoesNotThrow(() -> LOGGER.atFatal().withLocation().withThrowable(new SQLException(MSG)).log("LOGGER Level.FATAL: {}", () -> delimiters));

      // set to fail: catch in test.log and continue as test passed %throwable
      assertDoesNotThrow(() -> mLog(Level.FATAL, new SQLException(MSG), () -> delimiters));
    }
  }

  // Log4j 2 API, Markers, <https://logging.apache.org/log4j/2.x/manual/markers.html>
  @Nested
  class LogSQLMarkersTest {
    
    private static final Marker SQL_MARKER = MarkerManager.getMarker("SQL");
    private static final Marker SELECT_MARKER = MarkerManager.getMarker("SQL_SELECT").setParents(SQL_MARKER);
    
    private static final Logger LOGGER = LogManager.getLogger(LogSQLMarkersTest.class);

    @ParameterizedTest(name = "pattern of {0}")
    @ValueSource(strings = { "f", "a", "t", "a", "l" })
    void test_log_sql_marker_query(String table) {
      Map<String, String> attr = Map.of("FName", "John", "MName", "J", "LName", "Jones");
      LOGGER.traceEntry(table, attr);

      LOGGER.debug(SELECT_MARKER, "SELECT * FROM {} WHERE pk_1 = {} ", table, formatCols(attr));

      var set = attr.entrySet();

      assertEquals(3, (long) set.size());

      LOGGER.traceExit(set);
    }

    private String formatCols(Map<String, String> cols) {
      StringBuilder sb = new StringBuilder();
      boolean first = true;
      for (Map.Entry<String, String> entry : cols.entrySet()) {
          if (!first) sb.append(", ");
          sb.append(entry.getKey()).append("=").append(entry.getValue());
          first = false;
      }
      return sb.toString();
    }
  }

  // API <https://logging.apache.org/log4j/2.x/log4j-api/apidocs/org/apache/logging/log4j/message/StructuredDataMessage.html>
  // StructuredDataMessage: an ID (max 32 characters), message, and type (max 32 characters)
  @Nested
  class LogStructuredDataMessageTest {

    private static final Logger LOGGER = LogManager.getLogger(LogStructuredDataMessageTest.class);

    @Tag("Broken")
    @ParameterizedTest(name = "pattern of {0}")
    @ValueSource(strings = { "i", "n", "f", "o" })
    void test_log_structured_data_message(String param) {
      String uuid = UUID.randomUUID().toString();

      // id=1 is an assumption (wrong as needs: StructuredDataId)
      StructuredDataMessage msg = new StructuredDataMessage("1", param, Level.INFO.name());
      msg.put(param, uuid);
      // events: directory, file or exception
      EventLogger.logEvent(msg);

      assertTrue(true);
      
    }
  }

  @Nested
  class LogSecurityOWASPTest {

    private static final Logger LOGGER = LogManager.getLogger(LogSecurityOWASPTest.class);

    @Tag("Broken")
    @ParameterizedTest(name = "pattern of {0}")
    @ValueSource(strings = { "i", "n", "f", "o" })
    void test_log_structured_data_message(String param) {
      String uuid = UUID.randomUUID().toString();
      String[] array = { param, uuid };

      // onMatch=XXXX
      LOGGER.info("Print command line arguments");
      SecurityUtil.logCommandLineArguments(array);
      
      LOGGER.info("Print shell environment variables");
      SecurityUtil.logShellEnvironmentVariables();
      
      LOGGER.info("Print Java system properties");
      SecurityUtil.logJavaSystemProperties();

      assertTrue(true);

    }
  }


}

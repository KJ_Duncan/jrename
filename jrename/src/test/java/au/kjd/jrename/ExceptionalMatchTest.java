package au.kjd.jrename;

import java.io.IOException;

import java.nio.file.AtomicMoveNotSupportedException;
import java.nio.file.FileSystemException;
import java.nio.file.Path;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExceptionalMatchTest {

  private static final Logger LOGGER = LogManager.getLogger();

  /* TEST: log4j2 garbage free object creation on formatTo(StringBuilder) method */
  @Test void formatTo() {
    LOGGER.error(new ExceptionalMatch(Path.of(""), new IOException("this and that")));
    LOGGER.warn( new ExceptionalMatch(Path.of(""), new FileSystemException("today and tomorrow")));
    LOGGER.fatal(new ExceptionalMatch(Path.of(""), new AtomicMoveNotSupportedException("i", "owe", "you")));
    LOGGER.fatal(new ExceptionalMatch(Path.of(""), null));
  }

  // implements org.apache.logging.log4j.message.Message
  @Tag("Undetermined")
  @Test
  void getFormattedMessage() {
  }

  @Tag("Undetermined")
  @Test
  void getFormat() {
  }

  @Tag("Undetermined")
  @Test
  void getParameters() {
  }

  @Tag("Undetermined")
  @Test
  void getThrowable() {
  }
}

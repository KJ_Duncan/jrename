package au.kjd.jrename;

import au.kjd.jrename.Rename.Walker;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import static org.junit.jupiter.api.Assertions.assertEquals;

class WalkerTest {
  
  private static final Logger LOGGER = LogManager.getLogger();

  Walker walker = new Walker();

  @Tag("Undetermined")
  @Test
  void preVisitDirectory() {
  }

  @Tag("Undetermined")
  @Test
  void visitFile() {
  }

  @Test
  void visitFileFailed(@TempDir Path tempDir) {
    LOGGER.info("given a Path and IOException, Walker.visitFileFailed() should log.warn to test.log.");
    
    FileVisitResult result = walker.visitFileFailed(tempDir, new IOException("testing"));
    assertEquals("CONTINUE", result.name());
  }

  @Tag("Undetermined")
  @Test
  void postVisitDirectory() {
  }
}


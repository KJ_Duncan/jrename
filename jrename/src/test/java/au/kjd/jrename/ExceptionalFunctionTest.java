package au.kjd.jrename;

import java.io.IOException;
import java.lang.SecurityException;
import java.nio.file.AtomicMoveNotSupportedException;
import java.nio.file.FileSystemException;
import java.nio.file.Path;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

class ExceptionalFunctionTest {

  ExceptionalFunction<Path, Path> mv() { return path -> { throw new AtomicMoveNotSupportedException("me", "go", "bang"); }; }

  ExceptionalFunction<Path, Path> posix() { return path -> { throw new IOException("it's you, not me"); }; }

  ExceptionalFunction<Path, Path> gof = posix().compose(mv());

  ExceptionalFunction<Path, Path> se() { return path -> { throw new SecurityException("don't catch me"); }; }

  @Test
  void test_AtomicMoveNotSupportedException_thrown() {
    Path path = Path.of("");
    assertThrows(AtomicMoveNotSupportedException.class, () -> mv().apply(path), "Thrown at call site");
  }

  /* instanceof
     Tudose C et al., 2020, Listing 14.15 LogPassengerExistsExceptionExtension class, Chpt 14, p.286-87, Junit in Action, Third Edition, Manning Publications. */
  @Test
  void test_IOException_subtype() {
    Path path = Path.of("");

    try { mv().apply(path); }

    catch (IOException a) {
      if (a instanceof FileSystemException b) {
        if (b instanceof AtomicMoveNotSupportedException c) { assertEquals("me -> go: bang", c.getMessage()); }
      }
      else fail("Pattern matching: AtomicMoveNotSupportedException all went wrong");
    }
  }

  @Test
  @Tag("Undetermined")
  void apply() {
  }

  @Test
  @Tag("Undetermined")
  void compose() {
  }

  @Test
  @Tag("Undetermined")
  void andThen() {
  }

  @Test
  @Tag("Undetermined")
  void identity() {
  }
}

package au.kjd.jrename;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import static java.lang.String.format;
import static java.nio.file.FileSystems.getDefault;
import static java.util.regex.Pattern.compile;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/*
ParameterizedTest(name = "Works correctly {0} on output report html"), build/reports/tests/test/index.html

src/test/resources/junit-platform.properties
> junit.jupiter.displayname.generator.default = org.junit.jupiter.api.DisplayNameGenerator$Standard

junit-user-guide-5.7.1, 2.3.2. Setting the Default Display Name Generator p.14
                        org.junit.jupiter.api.DisplayNameGenerator.Standard
------------------------------------------------------------
Simone, N 2020, Sinnerman - Nina Simone Original Jazz Classics, Spotify, viewed 29 April 2021,
                <https://open.spotify.com/album/1hRTFjs1lKeW024WPpUyo6?si=0m6i8Da1SW2r6eGcf6c65g>
------------------------------------------------------------
$ ./gradlew test --tests "au.kjd.jrename.RenameTest"
> thanks gradle/junit
------------------------------------------------------------

replace("(_.)", ".").replace("(_)$", "")
 */
class RenameTest {

  // glob:*.txt == ^[^/]*\.txt$
  // glob:/**/*.txt == /tmp/junit[0-9]+/target-[0-9]+.txt
  // Oracle 2021, FileSystem.getPathMatcher, Java SE 16, viewed 17 May 2021, <https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/nio/file/FileSystem.html>

  final PathMatcher SYNTAX = getDefault().getPathMatcher(format("%s:%s", "glob", "/**/*.txt"));

  final String NONE = "";
  final String UNDERSCORE  = "_";

  final Pattern REMOVE__PREFIX        = compile("^(_)+");
  final Pattern REMOVE_TRAILING_      = compile("(_)+$");
  final Pattern REMOVE___BEFORE_EXT   = compile("(_)+(?=\\.)");
  final Pattern DUPLICATE_GREEDY      = compile("\\B(_)+\\B");
  final Pattern BLACK_LIST            = compile("[\\s!*#$%&'()+,;=@\\[\\]^`{}~|?]");
  final Pattern WHITE_LIST            = compile("[^0-9A-Z_a-z]");
  final Pattern PATTERN               = compile("[^\\-.0-9A-Z_a-z]");
  final Pattern POST_PATTERN_INCLUDES = compile("[0-9A-Z_a-z]+");
  final Pattern PATTERN_GROUPS        = compile("^(_)+\\B|\\b(?<=\\.)(_)+\\B|\\B(_)+(?=\\.)\\b|(_)+$");
  // ref: <https://regex101.com/r/vrPDK9/3>

  @Nested
  class Parameterised_pattern {

    @ParameterizedTest(name = "pattern of {0}")
    @ValueSource(strings = { " ", "!", "*", "#", "$", "%", "&", "'", "(", ")", "+", ",", ";", "=", "@", "[", "]", "^", "`", "{", "}", "~", "|", "?" })
    void test_black_list(String delimiters) { assertTrue(BLACK_LIST.matcher(delimiters).find()); }

    @ParameterizedTest(name = "pattern of {0}")
    @ValueSource(strings = { " ", "!", "*", "#", "$", "%", "&", "'", "(", ")", "+", ",", "-", ";", "=", "@", "[", "]", "^", "`", "{", "}", "~", "|", "?" })
    void test_white_list(String delimiters) { assertTrue(WHITE_LIST.matcher(delimiters).find()); }
  }

  @Nested
  class Parameterised_file_name {

    final Function<String, Boolean> hasToken = source -> PATTERN.matcher(source).find();

    final Function<String, String> rename = source -> PATTERN.matcher(source).replaceAll(UNDERSCORE);

    final Function<String, String> dedupe = source -> DUPLICATE_GREEDY.matcher(source).replaceAll(UNDERSCORE);

    final Function<String, String> groups = source -> PATTERN_GROUPS.matcher(source).replaceAll(NONE);

    @ParameterizedTest(name = "{index}> {0}")
    @MethodSource
    void test_stream_of_strings_with_delimiters(String fileName) {
      /* seeing whats what for the gradle build report layout */
      assertAll("File name",
         () -> assertTrue(WHITE_LIST.matcher(fileName).find()),
         () -> {
            String newFileName = (hasToken.apply(fileName)) ? rename.andThen(groups)
                                                                    .andThen(dedupe)
                                                                    .apply(fileName)
                                                            : fileName;

           System.out.println(newFileName);

            assertAll("New file name",
                      () ->  assertNotNull(newFileName),
                      () ->  assertNotEquals(fileName, newFileName));

            assertAll("New file name post patterns",
                      () -> assertFalse(BLACK_LIST.matcher(newFileName).find()),
                      () -> assertTrue(POST_PATTERN_INCLUDES.matcher(newFileName).find()));

            assertAll("New file name no prefix or trailing delimiters",
                      () -> assertFalse(REMOVE___BEFORE_EXT.matcher(newFileName).find()),
                      () -> assertFalse(REMOVE__PREFIX.matcher(newFileName).find()),
                      () -> assertFalse(REMOVE_TRAILING_.matcher(newFileName).find()));
         }
      );
    }

    /* MethodSource shares the static functions name or declares it in the annotated test @MethodSource("declaresName")
       ((\.[A-Za-z~][A-Za-z0-9~]*)*) - 30.3.3 Special handling of file extensions p.250 GNU Coreutils v8.32 2020
       FIXME: Standardised characters for a file names (regex: white-list), file max/min length, can do file extensions (else: diy log msg), remove trailing: filename_, filename_.ext
       Charsets <https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/nio/charset/package-summary.html>
       Character <https://docs.oracle.com/en/java/javase/16/docs/api/java.base/java/lang/Character.html>
       ? <- forgot it, so will go with a white-list pattern
     */
    static Stream<String> test_stream_of_strings_with_delimiters() {
      return Stream.of("a file name",
                       "a file name.",
                       "a file - name.ext",
                       "a  file!|_name.ext",
                       "__a file!name#.ext.gz",
                       "~a file!_._name.#ext",
                       "_a._file!name#.ext~",
                       "regex:=characters+internationalisation?.ext");
    }
  }

  @Nested
  class File_nio {

    /* testMethod(@TempDir Path tempDir) ensures isolation on file creation: walk(tmp, depth).count() */

    @Test
    @DisplayName("run: catches IllegalArgumentException errors should equal 1 and returns an Stream.empty")
    void testRunCatchesExceptions(@TempDir Path tempDir) {

      /* throws IllegalArgumentException */
      int depth = -1;
      int errors = 0;

      Stream<Path> result = Stream.empty();

      try (Stream<Path> stream = Files.walk(tempDir, depth)) {

        result = stream.filter(path -> SYNTAX.matches(path) &&
                                       WHITE_LIST.asPredicate()
                                                 .test(path.getFileName().toString())); }

      catch (IllegalArgumentException|
             SecurityException|
             IOException boom) { errors++; }

      assertEquals(0L, result.count());
      assertEquals(1, errors);
    }

    @Test
    @DisplayName("run: walks a file tree at depth 0 and Stream<Path> should equal 1")
    void testRunWalksFileTreeAtDepthZero(@TempDir Path tempDir) {
      /*
        The stream walks the file tree as elements are consumed.
        The Stream returned is guaranteed to have at least one element,
        the starting file itself.
       */
      int depth = 0;
      int errors = 0;

      long result = 0;

      try (Stream<Path> stream = Files.walk(tempDir, depth)) {

        result = stream.count(); }

      catch (IllegalArgumentException|
             SecurityException|
             IOException boom) { errors++; }

      assertEquals(0, errors);
      assertEquals(1L, result);
    }

    @Test
    @DisplayName("run: walks a file tree at depth 1 and Stream<Path> should equal 2")
    void testRunWalksFileTreeAtDepthOne(@TempDir Path tempDir) throws IOException {

      Files.createTempFile(tempDir, "target-", ".txt");

      int depth = 1;
      int errors = 0;

      long result = 0;

      try (Stream<Path> stream = Files.walk(tempDir, depth)) {

        result = stream.count(); }

      catch (IllegalArgumentException|
             SecurityException|
             IOException boom) { errors++; }

      assertEquals(0, errors);
      assertEquals(2L, result);
    }

    @Test
    @DisplayName("run: walks a file tree at depth 1 and filters Stream<Path> should equal 1")
    void testRunWalksFileTreeAtDepthTwoAndFilter(@TempDir Path tempDir) throws IOException {

      Files.createTempFile(tempDir, "target-", ".txt");

      int depth = 1;
      int errors = 0;

      long result = 0;

      try (Stream<Path> stream = Files.walk(tempDir, depth)) {

        /* Predicate should match and returns 1 */
        result = stream.filter(path -> SYNTAX.matches(path) &&
                                       WHITE_LIST.asPredicate()
                                                 .test(path.getFileName().toString())).count(); }

      catch (IllegalArgumentException|
             SecurityException|
             IOException boom) { errors++; }

      assertEquals(0, errors);

      /* FileTreeIterator.hasNext: if walker is closed throw IllegalStateException
         intellij debugged it, and not afraid to admit it (thanks guys)
       */
      assertEquals(1L, result);
    }
  }
}

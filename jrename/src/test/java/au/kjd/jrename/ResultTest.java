package au.kjd.jrename;

import java.util.function.LongSupplier;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.ThrowingSupplier;

import static java.lang.Long.parseLong;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.fail;

/*
   $ ./gradlew test --tests "au.kjd.jrename.ExceptionalResultTest"
   > thanks gradle
 */
class ExceptionalResultTest {

  @Tag("Broken")
  @Test
  void result_is_the_declaring_class() {
    var s = new ESuccess<>(2L);
    var f = new EFailure<>(new Throwable("boom"));
    assertSame(ExceptionalResult.class, s.getClass());
    assertSame(ExceptionalResult.class, f.getClass());
  }

  @Test
  void success_is_not_null_and_value_equals_1() {
    var sucs = new ESuccess<>(1L);
    assertNotNull(sucs);
    assertEquals(1L, sucs.success());
  }

  @Test
  void failure_is_not_null_and_does_not_throw_exception_and_value_equals_boom() {
    var fails = new EFailure<>(new Throwable("boom"));
    assertNotNull(fails);
    assertDoesNotThrow(fails::failure);
    assertEquals("boom", fails.failure().getMessage());
  }

  @Test
  void resultFrom_returns_instanceof_Success_given_1L() {

    LongSupplier lazier = () -> 1L;

    var result = ExceptionalResult.resultFrom(lazier);

    assertSame(ESuccess.class, result.getClass());

    if (result instanceof ESuccess s) assertEquals(1L, s.success());
    else fail("DID NOT MATCH: result instanceof Success s");

    // casting this way is also possible
    assertEquals(1L, ((ESuccess) result).success());
  }

  @Test
  void resultFrom_returns_instanceof_Failure_given_NumberFormatException() {

    /* NOTE: that neither the character L ('\u004C') nor l ('\u006C') is permitted to appear at the end
             of the string as a type indicator (ref: java.lang.Long.parseLong) */

    LongSupplier lazier = () -> parseLong("1\u004C");

    var result = ExceptionalResult.resultFrom(lazier);

    assertNotNull(result);
    assertDoesNotThrow((ThrowingSupplier<Throwable>) ((EFailure) result)::failure);

    // <java.lang.NumberFormatException: For input string: \"1L\">
    if (result instanceof EFailure f) assertEquals("For input string: \"1L\"", f.failure().getMessage());
    else fail("DID NOT MATCH: result instanceof Failure f");
  }

  @Test
  @Tag("Playground")
  void playground_for_pattern_match_experiments() {

    LongSupplier a = () -> 1L;
    LongSupplier b = () -> parseLong("1\u004C");

    var resultS = ExceptionalResult.resultFrom(a);
    var resultF = ExceptionalResult.resultFrom(b);

    assertNotNull(resultS);
    assertNotNull(resultF);

    assertSame(ESuccess.class, resultS.getClass());
    assertSame(EFailure.class, resultF.getClass());

   /*
    * Oracle 2021, JEP 394: Pattern Matching for instanceof, OpenJDK, viewed 12 May 2021, <https://openjdk.java.net/jeps/394>
    *
    * Bierman, G & Goetz, B 2018, Pattern Matching for Java, viewed 19 May 2021, <https://cr.openjdk.java.net/~briangoetz/amber/pattern-match.html>
    *
    * observation: f is not bound on negation to the inner scope
    * observation: f is bound on negation to the outer scope
    *
    * experiment: casting outer scope after negation
    *
    * precondition: sealed interface
    * experiment: var r = (Result<?, ?>)resultFrom(() -> 1L);
    *             if (r instanceof Success s) s.success()
    *             default cannot be referenced from a static context
    *
    * C = A|B
    * Bool = True|False
    * Result = Success|Failure
    *
    * sealed interface Result permits Success|Failure
    *   where Success = final static record
    *         Failure = final static record
    *
    * experiment: default and static methods in Result
    *
    * !(Result instanceof Success s) {
    *   is <s>.defaultMethod() bounded to the Result type on negation?
    *   no as <s> is an uninitialised
    *   so why are we testing for false if false gives me anything but what I need.
    * }
    * but then <s> is initialised as true in the outer scope, so why did I just test for false?
    *
    * Need to walk through someone elses code base to see how they might use the negation block.
    *
    * JEP 394: Future Work -- will enhance on the teamwork done in Haskell for Pattern Matching
    *
    * Public, E 1990, Brothers Gonna Work It Out, Def Jam Recordings, Spotify, viewed 16 May 2021,
    *                 <https://open.spotify.com/track/6wSjsP4iMh2ZkKbW1QK7i0?si=1f7e45f27dd549b8>
    */
   if (!(resultS instanceof EFailure f)) { return; }
   f.failure();
   ((EFailure) resultS).failure();
  }
}
